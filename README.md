# Questionnaire package

This package provides a full questionnaire widget which means that the user should provide the questions, colors and texts.  
The results are notified back by a delegate.

## Pubspec.yaml

To use this package you have to add the following line to your pubspec.yaml:

questionnaire:  
        path: ../../packages/questionnaire

Take into account that the project which uses the package is on the same level as the packages folder.  
If not you have to adjust the path to questionnaire.

## Import the Package

Use the following import in the file where you want to use the StartQuestionnairePage widget:

import 'package:questionnaire/questionnaire.dart';

Make sure you add 'with OnQuestionnaireMixin' to your State class and make sure the corresponding functions are defined:

@override
  void onQuestionnaireComplete(Map<int, QuestionResultBase> answers){}

@override
  void onQuestionComplete(QuestionResultBase answer){}

## StartQuestionnairePage

The actual widget is named StartQuestionnairePage and requires four parameters:

### 1. List\<QuestionBase\> questionList;

Here the user can define a list of questions by using one or more of the following definition classes:

- NormalQuestionDefinition, which represents a question with a questiontext, a hinttext and some conditions.
- SingleChoiceQuestionDefinition which represents a list of choices with optionally a textfield per choice. The user can only select one option.
- MultipleChoiceQuestionDefinition which represents a list of choices with optionally a textfield per choice. The user is able to select multiple options.
- PersonInfoQuestionDefinition which represents a custom page where the user can fill in information for one or more persons including their relations. This page will get input from other questions !!
- PersonEducationChoiceQuestion which represents a custom page, based on the MultipleChoiceQuestionDefinition, where the choice texts are retrieved from a different question answer !!

```
class NormalQuestionDefinition {
  final int questionNumber;             // required.
  final String questionText;            // required.
  final String hintText;                // required.
  final TextInputType keyboardType;     // required.
  final String numberValidationText;    // optional; eg. 'Please enter a number'
  final Map<String, int> conditions;    // optional; eg: conditions['1': 3], meaning: if the answer is 1 then go to question 3. Also use 'Route':3 to route to question 3 regardless of any input. Use 'Other':3 to simulate routing conditions to any other input in the list.
}
```

```
class SingleChoiceQuestionDefinition {
  final int questionNumber;                 // required.
  final String questionText;                // required.
  final List<ChoiceDefinition> choiceList;  // required, fill in a ChoiceDefinition for every choice (see ChoiceDefinition)
  final String hintText;                    // optional.
  final int? nextQuestion                   // optional, fill in if question has to redirect to a specific other question.
  final Map<int, List<int>> hideConditions; // optional; eg: hideConditions {1: [2, 1, 2, 4]}, meaning: if the answer from question number 1 is 2, then hide the choices with number 1, 2 and 4.
}
```

```
class MultipleChoiceQuestionDefinition {
  final int questionNumber;                 // required.
  final String questionText;                // required.
  final List<ChoiceDefinition> choiceList;  // required, fill in a ChoiceDefinition for every choice (see ChoiceDefinition)
  final String hintText;                    // optional.
}
```

```
class ChoiceDefinition {
  final int choiceNumber;                 // required.
  final String choiceText;                // required.
  final bool addTextField;                // optional, default = false.
  final String textFieldHint;             // optional.
  final bool resetOtherFields;            // optional, if yes, reset other multi choices when clicked.
}
```

```
class PersonInfoQuestionDefinition {
  final int questionNumber;                     // required.
  final String questionText;                    // required.
  final String questionText2;                   // required.
  final String questionText3;                   // required.
  final int questionNumberPersons;              // required.
  final String titleText;                       // required.
  final List<ChoiceDefinition> relationChoices; // required.
  final Map<int, List<int>> relationMap;        // required.
  final List<ChoiceDefinition> genderChoices;   // required.
  final String hintText;                        // optional.
}
```

```
class PersonEducationChoiceQuestion {
  final int questionNumber;                 // required.
  final String questionText;                // required.
  final int questionNumberPersons           // required.
  final String hintText;                    // optional.
}
```

### 2. Map\<String, Color\> colorMap;

Here the user has to provide a list of colors for the questionnaire page.

- colorMap[questionnaireCaptionColorText] = Caption Color
- colorMap[questionnaireButtonColorText] = Button Color
- colorMap[questionnaireGreyButtonColorText] = Grey Button Color
- colorMap[questionnairePrimaryTextColorText] = Primary Text Color
- colorMap[questionnaireBackgroundColorText] = Background Color
- colorMap[questionnaireSaveButtonColorText] = Save Button Color

### 3. Map\<String, String\> textMap;

Here the user has to provide a list of texts for the questionnaire page.

- textMap[questionnaireCaptionText] = Caption Text
- textMap[questionnaireQuestionText] = Question Translation Text
- textMap[questionnaireSaveButtonText] = Save Button Text
- textMap[questionnaireNextButtonText] = Next Button Text
- textMap[questionnaireConfirmButtonText] = Confirmation Button Text

These texts should only be applied if the PersonInfoQuestionDefinition is used !!

- textMap[questionnairePersonInfoGenderHintText] = Person Info Gender Text
- textMap[questionnairePersonInfoGenderDialogText] = Person Info Gender Dialog Text
- textMap[questionnairePersonInfoDateOfBirthHintText] = Person Info Date Of Birth Hint Text
- textMap[questionnairePersonInfoDateOfBirthText] = Person Info Date Of Birth Text
- textMap[questionnairePersonInfoRelationHintText] = Person Info Relation Hint Text
- textMap[questionnairePersonInfoRelationText] = Person Info Relation Text
- textMap[questionnairePersonInfoYourSelfText] = Person Info Yourself Text
- textMap[questionnaireLastPageLine1Text] = Last Page Text for Line 1
- textMap[questionnaireLastPageLine2Text] = Last Page Text for Line 2
- textMap[questionnaireLastPageLine3Text] = Last Page Text for Line 3
- textMap[questionnaireAnswersSentText] = Last Page Answers Sent Text
- textMap[questionnaireCalendarDateInThePastText] = Calendar Date in the Past Text

These texts are added for other pages.

- textMap[questionnaireLastPageEndText] = Title for final page

### 4. OnQuestionnaireCompleteMixin delegate;

Here the user has to provide the class instance where the delegate can call the onQuestionnaireComplete function on.


## Answers

The function onQuestionComplete(QuestionResultBase answer) is called after a question is answered. This is needed to store the answers for the resume functionality !

If QuestionType = normal, then the answer is a NormalResult (see below).  
If QuestionType = singlechoice, then the answer is a SingleChoiceResult (see below).  
If QuestionType = multichoice, then the answer is a List\<MultiChoiceResult\> (see below).   
If QuestionType = personinfo, then the answer is a PersonInfoResult (see below).
If QuestionType = personeducation, then the answer is a List\<MultiChoiceResult\> (see below).

The function onQuestionnaireComplete(Map<int, Object> answers) is called when the user has clicked the Save button after all questions are answered.

The answers Map contains all the answers.  
The key represents the question number.  
The value depends on the QuestionType:  

If QuestionType = normal, then the value is a NormalResult (see below).  
If QuestionType = singlechoice, then the value is a SingleChoiceResult (see below).  
If QuestionType = multichoice, then the value is a List\<MultiChoiceResult\> (see below).   
If QuestionType = personinfo, then the value is a PersonInfoResult (see below).
If QuestionType = personeducation, then the value is a List\<MultiChoiceResult\> (see below). 

```
class NormalResult {
  final int questionNumber; // represents the question number.          
  String answer;            // represents the value from the textfield.                    
}
```

```
class SingleChoiceResult {
  final int questionNumber; // represents the question number.   
  final int choiceNumber;   // represents the choice number.             
  int value;                // represents the clicked radio button number.                
  String textField;         // represents the inputted text, if it is there !.     
}
```

```
class MultiChoiceResult {
  final int questionNumber; // represents the question number.   
  final int choiceNumber;   // represents the choice number.                
  bool value;               // represents a bool if this checkbox is clicked or not.             
  String textField;         // represents the inputted text, if it is there !.     
}
```

```
class PersonInfoResult {   
  final int questionNumber; // represents the question number.     
  List<Person> personList;  // represents the inputted text, if it is there !.     
}

class Person {        
  final int personNumber;     // represents the person number.
  final String gender;        // represents the gender.
  final DateTime dateOfBirth; // represents the date of birth.
  String relation;            // optional, represents the relation to the first person !
}
```
