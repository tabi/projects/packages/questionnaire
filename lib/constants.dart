import 'package:flutter/material.dart';

const questionnaireCaptionColorText = 'captionColor';
const questionnaireBlueButtonColorText = 'buttonColor';
const questionnairePrimaryTextColorText = 'primaryTextColor';
const questionnaireBackgroundColorText = 'backgroundColor';
const questionnaireGreyButtonColorText = 'greyBackgroundColor';
const questionnaireGreenButtonColorText = 'saveButtonColor';

const questionnaireCaptionText = 'captionText';
const questionnaireQuestionText = 'questionText';
const questionnaireIntroText = 'questionIntroText';
const questionnaireSaveButtonText = 'saveButtonText';
const questionnaireNextButtonText = 'nextButtonText';
const questionnaireStartButtonText = 'startButtonText';
const questionnaireConfirmButtonText = 'confirmButtonText';
const questionnaireLastPageEndText = 'lastPageEndText';
const questionnaireLastPageLine1Text = 'lastPageLine1Text';
const questionnaireLastPageLine2Text = 'lastPageLine2Text';
const questionnaireLastPageLine3Text = 'lastPageLine3Text';
const questionnaireIntroductionPageLine1Text = 'introductionPageLine1Text';
const questionnaireIntroductionPageLine2Text = 'introductionPageLine2Text';
const questionnaireIntroductionPageLine3Text = 'introductionPageLine3Text';
const questionnaireAnswersSentText = 'answersSentText';
const questionnaireCalendarDateInThePastText = 'calendarDateInThePastText';

const questionnairePersonInfoYourSelfText = 'yourselfText';
const questionnairePersonInfoGenderHintText = 'genderHintText';
const questionnairePersonInfoGenderDialogText = 'genderDialogText';
const questionnairePersonInfoDateOfBirthHintText = 'dateOfBirthHintText';
const questionnairePersonInfoDateOfBirthText = 'dateOfBirthText';
const questionnairePersonInfoRelationHintText = 'relationHintText';
const questionnairePersonInfoRelationText = 'relationText';

const questionTopGreyText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 18,
  color: Color(0xFFC4C4C4),
);

const questionMainTitleText = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 20,
  color: Color(0xFF33425B),
);

const questionBaseText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Color(0xFF33425B),
);

const questionSubBaseText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: Color(0xFF33425B),
);

const captionText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w300,
  fontSize: 20,
  fontStyle: FontStyle.italic,
  color: Color(0xFF33425B),
);

const hintText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 16,
  color: Color(0xFFAAAAAA),
);

const optionText = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Color(0xFF33425B),
);

const buttonText18 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 18,
  color: Color(0xFFFFFFFF),
);

const buttonText16 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 16,
  color: Color(0xFFFFFFFF),
);

const errorText16 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Color(0xFFE94C0A),
);

class ColorReel {
  static const Color darkBlueish = Color(0xFF33425B); // Only used for charts
  static const Color darkGreen = Color(0xFF488225); // Only used for charts
  static const Color darkTextColor = Color(0xFF33425B);
  static const Color lightBlueWithOpacity = Color(0xFF26AFD5); //Lightblue, used only for login screen
  static const Color lightGray = Color(0xFFDDDDDD);
  static const Color veryDarkGray = Color(0xFFE5E5E5);
  static const Color lightGreen = Color(0xFFAFCB05); // Used in calendar and login
  static const Color midblue = Color(0xFF271D6C); //Only used for the charts
  static const Color midGray = Color(0xFFAAAAAA);
  static const Color orange = Color(0xFFF39200); // Used in calendar
  static const Color orangeDark = Color(0xFFDB7758); // Used in calendar
  static const Color pink = Color(0xFFAF0E80); //Pink used for cost and add screens
  static const Color primaryColor = Color(0xFF00A1CD); //Lightblue, used for navigation, topbar, etc.
  static const Color veryLightBlue = Color(0xFFE5F6FA); //Very lightblue used for calendar, and search text
  static const Color veryLightGray = Color(0xFFEFEFEF);
  static const Color lightGrayishBlue = Color(0xFFEBEEF0);
}
