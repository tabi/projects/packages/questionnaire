import 'package:flutter/widgets.dart';

mixin FocusAndTextControllerMixin {
  late List<TextEditingController?> textEditingControllers;
  late List<FocusNode?> textFocusNodes;

  void createFocusAndTextControllers({required int length}) {
    textEditingControllers =
        List<TextEditingController?>.filled(length, null);
    textFocusNodes = List<FocusNode?>.filled(length, null);
  }

  void disposeFocusAndTextControllers({int? length}) {
    for (var c = 0; c != length; c++) {
      textFocusNodes[c]?.dispose();
      textEditingControllers[c]?.dispose();
    }
  }
}
