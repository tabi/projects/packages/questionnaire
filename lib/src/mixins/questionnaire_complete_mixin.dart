import '../definitions/question_result_base.dart';

mixin OnQuestionnaireMixin {
  void onQuestionnaireComplete(
    // key is question number.
    // value depends on type of question:
    // when type = normal, value is NormalResult.
    // when type = singlechoice, value is SingleChoiceResult.
    // when type = multichoice, value is list of MultiChoiceResult.
    // when type = personinfo, value is PersonInfoResult.
    // when type = personeducation, value is list of MultiChoiceResult.
    Map<int, QuestionResultBase> answers,
  );

  void onQuestionComplete(
    // when type = normal, answer is NormalResult.
    // when type = singlechoice, answer is SingleChoiceResult.
    // when type = multichoice, answer is list of MultiChoiceResult.
    // when type = personinfo, answer is PersonInfoResult.
    // when type = personeducation, answer is list of MultiChoiceResult.
    QuestionResultBase answer,
  );
}
