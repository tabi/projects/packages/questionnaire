import 'package:flutter/animation.dart';
import 'package:flutter/widgets.dart';

mixin AnimationMixin {
  late AnimationController animationController;
  Animation<double>? opacityAnimation;

  void createAnimation({required TickerProvider tickerprovider}) {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: tickerprovider);
    opacityAnimation =
        Tween<double>(begin: 0.2, end: 1.0).animate(animationController);
  }
}