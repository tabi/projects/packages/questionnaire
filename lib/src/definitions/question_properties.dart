import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../widgets/final_page.dart';
import 'pagination_properties.dart';
import 'question_base.dart';

// used to keep track of questionnaire wide variables.
// the QuestionProperties are 'made available' by the Provider package.
class QuestionProperties extends ChangeNotifier {
  // list contains the complete question list
  List<QuestionBase>? _questionList;

  List<PaginationProperties>? _questionVisibilityProperties;

  // list contains the GlobalKeys for every question
  List<GlobalKey>? _keys;

  // is used to reset the question list after one or more questions are removed
  var _backupQuestionList = List<QuestionBase>.empty(growable: true);

  // is used to reset the keys list after one or more keys are removed
  var _backupKeys = List<GlobalKey>.empty(growable: true);

  // contains the indices of the removed questions
  var _removedQuestions = List<int>.empty(growable: true);

  // represents the current index
  var _index = 0;

  // represents the total number of widgets
  var _totalWidgetCount = 0;

  QuestionProperties(List<QuestionBase> questionList, int latestIndexIncompleteQuestionnaire) {
    // add last page to question list
    var newList = List<QuestionBase>.empty(growable: true);
    newList.addAll(questionList);
    newList.add(FinalPage(
      buttonText: '',
      buttonColor: Colors.white,
      questionEndText: '',
      line1Text: '',
      line2Text: '',
      line3Text: '',
      answersSentText: '',
      onComplete: toString,
    )); // this is a dummy object !

    _setQuestionList(newList, latestIndexIncompleteQuestionnaire);
  }

  List<QuestionBase> get getQuestionList {
    return [..._questionList!];
  }

  List<PaginationProperties> get getPageProperties {
    return [..._questionVisibilityProperties!];
  }

  int get getCurrentIndex {
    return _index;
  }

  int get getTotalWidgetCount {
    return _totalWidgetCount;
  }

  // represents the index of the highest active question
  int get highestIndex {
    var idx = _questionVisibilityProperties!.indexWhere((element) => element.isEnabled == false);

    if (idx != -1) {
      return idx - 1;
    } else {
      return _totalWidgetCount - 1;
    }
  }

  bool get hasRemovedPages {
    return _removedQuestions.length > 0;
  }

  List<GlobalKey>? get getKeys {
    return [..._keys!];
  }

  void resetQuestionList() {
    _questionList!.clear();

    for (var q in _backupQuestionList) {
      _questionList!.add(q);
    }

    _keys!.clear();

    for (var key in _backupKeys) {
      _keys!.add(key);
    }

    _totalWidgetCount = _questionList!.length;

    _questionVisibilityProperties!.clear();
    _resetVisibilityProperties(0);

    _removedQuestions.clear();
    notifyListeners();
  }

  void _resetVisibilityProperties(int latestIndexIncompleteQuestionnaire) {
    if (latestIndexIncompleteQuestionnaire == 0) {
      for (var item = 0; item < _totalWidgetCount; item++) {
        var pp = item == 0 ? PaginationProperties(isActive: true, isEnabled: true) : PaginationProperties(isActive: false, isEnabled: false);
        _questionVisibilityProperties!.add(pp);
      }
    } else {
      for (var item = 0; item < _totalWidgetCount; item++) {
        if (item <= latestIndexIncompleteQuestionnaire) {
          _questionVisibilityProperties!.add(PaginationProperties(isActive: false, isEnabled: true));
        } else if (item == latestIndexIncompleteQuestionnaire + 1) {
          _questionVisibilityProperties!.add(PaginationProperties(isActive: true, isEnabled: true));
        } else {
          _questionVisibilityProperties!.add(PaginationProperties(isActive: false, isEnabled: false));
        }
      }
    }
  }

  void _setQuestionList(List<QuestionBase> questionList, int latestIndexIncompleteQuestionnaire) {
    _questionList = questionList;
    _totalWidgetCount = _questionList!.length;
    for (var question in questionList) {
      _backupQuestionList.add(question);
    }

    _keys = List.generate(questionList.length, (index) => GlobalKey());

    for (var key in _keys!) {
      _backupKeys.add(key);
    }

    _questionVisibilityProperties = List<PaginationProperties>.empty(growable: true);
    _resetVisibilityProperties(latestIndexIncompleteQuestionnaire);

    notifyListeners();
  }

  void setActivePage(int index) {
    // first reset current active page
    var res = _questionVisibilityProperties!.firstWhere((element) => element.isActive == true, orElse: null);
    res.active = false;

    _questionVisibilityProperties![index].active = true;
    _questionVisibilityProperties![index].enable = true;

    // make sure the pages left of the 'active' page are enabled !
    for (var page = 0; page < index; page++) {
      _questionVisibilityProperties![page].enable = true;
    }
  }

  void setCurrentIndex(int index, {bool notification = true}) {
    _index = index;

    if (notification) {
      notifyListeners();
    }
  }

  void removeQuestion(int index) {
    _questionList!.removeAt(index);
    _keys!.removeAt(index);
    _questionVisibilityProperties!.removeAt(index);
    _removedQuestions.add(index);
    _totalWidgetCount = _questionList!.length;
    notifyListeners();
  }

  void removeQuestions(int startIndex, int endIndex) {
    _questionList!.removeRange(startIndex, endIndex);
    _keys!.removeRange(startIndex, endIndex);
    _questionVisibilityProperties!.removeRange(startIndex, endIndex);

    for (var cnt = startIndex; cnt < endIndex; cnt++) {
      _removedQuestions.add(cnt);
    }
    _totalWidgetCount = _questionList!.length;
    notifyListeners();
  }
}
