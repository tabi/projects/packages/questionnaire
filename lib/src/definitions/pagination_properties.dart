

// This class represents the visibility properties of the pagination widget pages.
class PaginationProperties {
  // only one page can be active at the same time ( represented by a blue color )
  bool _isActive;

  // value is true when the page is filled in or active ( represented by a black number ).
  // when the value is false the display is represented by a grey number.
  bool _isEnabled;

  bool get isActive {
    return _isActive;
  }

  set active(bool value) {
    _isActive = value;
  }

  bool get isEnabled {
    return _isEnabled;
  }

  set enable(bool value) {
    _isEnabled = value;
  }

  PaginationProperties({required isActive, required isEnabled})
      : _isActive = isActive,
        _isEnabled = isEnabled;

  @override
  String toString() {
    return "Page Property, isActive [$_isActive] and isEnabled [$isEnabled]";
  }
}
