// base class for every question result.
// contains parameters which are needed for every result.
abstract class QuestionResultBase {
  int? get questionNumber;
}