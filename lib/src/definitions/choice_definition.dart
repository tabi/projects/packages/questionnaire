

// represents a choice within a single- or multiple choice question
class ChoiceDefinition {
  // contains the choice number
  final int _choiceNumber;

  // contains the choice text
  final String _choiceText;

  // adds an additional textfield next to the choice when true. Default is false.
  final bool _addTextField;

  // represents the visibility flag of the choice. Default is true.
  bool _visible;

  // optional field represents the hinttext for the textfield.
  final String? _textFieldHint;

  // optional field represents if this field is clicked, other fields should be reset !
  final bool? _resetOtherFields;

  // optional field represents the next question number, used when this choice is chosen in a single choice question.
  final int? _nextQuestionNumber;

  int get choiceNumber {
    return _choiceNumber;
  }

  String get choiceText {
    return _choiceText;
  }

  bool get addTextField {
    return _addTextField;
  }

  String? get textFieldHint {
    return _textFieldHint;
  }

  bool? get resetOtherFields {
    return _resetOtherFields;
  }

  int? get nextQuestionNumber {
    return _nextQuestionNumber;
  }

  bool getVisible() {
    return _visible;
  }

  void setVisible(bool value) {
    _visible = value;
  }

  @override
  String toString() {
    return 'ChoiceDefinition choiceNumber [$choiceNumber], text [$choiceText], texField [$addTextField], texFieldHint [$textFieldHint], resetOtherFields [$resetOtherFields], next question [$nextQuestionNumber] and visibility [${getVisible()}]';
  }

  ChoiceDefinition({
    required choiceNumber,
    required choiceText,
    visible = true,
    addTextField = false,
    textFieldHint,
    resetOtherFields,
    nextQuestionNumber,
  })  : _choiceNumber = choiceNumber,
        _choiceText = choiceText,
        _visible = visible,
        _addTextField = addTextField,
        _textFieldHint = textFieldHint,
        _resetOtherFields = resetOtherFields,
        _nextQuestionNumber = nextQuestionNumber;
}
