// represents the supported question types
enum QuestionType {
  // normal question with a textfield.
  normal,

  address,

  birthday,

  numberplate,

  // single choice question with radio buttons and optional textfields.
  singlechoice,

  // multiple choice question with checkboxes and optional textfields.
  multichoice,

  // multiple choice question with buttons including icon and text.
  multichoice_icon,

  // person info question with dynamically created controls depending on answers from other questions.
  personinfo,

  // person education question with dynamically created text depending on answers from other questions.
  personeducation,

  // dummy to represent the last page !
  lastpage,
}

// base class for every question definition.
// contains parameters which are needed for every question.
abstract class QuestionBase {
  QuestionType get type;
  bool get hasNextButton;
  int get questionNumber;
  String get questionText;
}
