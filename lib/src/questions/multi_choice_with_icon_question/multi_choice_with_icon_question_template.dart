import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../definitions/choice_definition.dart';
import '../../mixins/animation_mixin.dart';
import '../../widgets/opacity_button.dart';
import '../multi_choice_question/multi_choice_question_result.dart';

class MultipleChoiceWithIconQuestion extends StatefulWidget {
  final List<MultiChoiceResultItem>? value;
  final ValueChanged<MultiChoiceResult?> questionAnswered;
  final ValueChanged<MultiChoiceResult?> questionReturned;
  final String questionTranslationText;
  final String questionNextTranslationText;
  final String questionText;
  final String? hintText;
  final List<ChoiceDefinition> choiceList;
  final List<IconData> iconList;
  final int questionNumber;
  final int numberInList;

  MultipleChoiceWithIconQuestion({
    Key? key,
    required this.questionNumber,
    required this.choiceList,
    required this.iconList,
    required this.questionText,
    required this.questionTranslationText,
    required this.questionNextTranslationText,
    required this.questionAnswered,
    required this.questionReturned,
    required this.numberInList,
    this.hintText,
    this.value,
  }) : super(key: key);

  @override
  _MultipleChoiceWithIconQuestionState createState() => _MultipleChoiceWithIconQuestionState();
}

class _MultipleChoiceWithIconQuestionState extends State<MultipleChoiceWithIconQuestion>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin, AnimationMixin {
  MultiChoiceResult? multiChoiceResult;
  var choiceValuesClicked = List<MultiChoiceResultItem>.empty(growable: true);

  late bool nextButtonActive;

  final scrollController = ScrollController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    nextButtonActive = false;
    createAnimation(tickerprovider: this);

    choiceValuesClicked = _createChoiceValues(widget.value);

    multiChoiceResult = MultiChoiceResult(questionNumber: widget.questionNumber, resultList: null);
  }

  @override
  void dispose() {
    choiceValuesClicked.clear();

    super.dispose();
  }

  List<MultiChoiceResultItem> _createChoiceValues(List<MultiChoiceResultItem>? choicesClicked) {
    for (var choice in widget.choiceList) {
      choiceValuesClicked.add(MultiChoiceResultItem(choiceNumber: choice.choiceNumber, value: false, textField: null));
    }

    if (choicesClicked != null && choicesClicked.length > 0) {
      choicesClicked.forEach((element) {
        var idx = choiceValuesClicked.indexWhere((el) => el.choiceNumber == element.choiceNumber);
        if (idx != -1) {
          choiceValuesClicked[idx].value = true;
          choiceValuesClicked[idx].textField = element.textField;
        }
      });
    }

    return choiceValuesClicked;
  }

  bool _choicesClicked() {
    bool result = false;
    if (choiceValuesClicked != null) {
      var clickedChoices = choiceValuesClicked.where((choice) => choice.value == true).toList();

      result = clickedChoices.length != 0;
    }

    return result;
  }

  void _resetChoicesClicked() {
    for (var choice in widget.choiceList) {
      choiceValuesClicked.add(MultiChoiceResultItem(choiceNumber: choice.choiceNumber, value: false, textField: null));
    }
  }

  void _clearAllChoices() {
    choiceValuesClicked.clear();
    _resetChoicesClicked();
  }

  void _clearResetFields() {
    for (var choice in widget.choiceList) {
      if (choice.resetOtherFields ?? false) {
        choiceValuesClicked[choice.choiceNumber - 1].value = false;
      }
    }
  }

  Function? _checkNextButtonPressed() {
    if (nextButtonActive) {
      return () {
        FocusScope.of(context).unfocus();
        widget.questionAnswered(_collectAnswerData());
      };
    } else {
      return null;
    }
  }

  MultiChoiceResult? _collectAnswerData() {
    multiChoiceResult!.setResultList(choiceValuesClicked.where((item) => item.value == true).toList());
    return multiChoiceResult;
  }

  void _processCheckLogic(bool val, ChoiceDefinition item) {
    if (val) {
      item.resetOtherFields ?? false ? _clearAllChoices() : _clearResetFields();
    }
  }

  void _processNextButtonActive(bool? val, ChoiceDefinition item) {
    setState(() {
      if (val!) {
        nextButtonActive = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return RawScrollbar(
      controller: scrollController,
      thumbColor: Colors.grey,
      thickness: 8,
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20),
              child: Text(
                '${widget.questionTranslationText} ${widget.numberInList}'.toUpperCase(),
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
              child: Text(
                widget.questionText,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            (widget.hintText != null)
                ? Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
                    child: Text(widget.hintText!, style: Theme.of(context).textTheme.caption),
                  )
                : Container(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                for (var itemIndex = 0; itemIndex < widget.choiceList.length; itemIndex++)
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20.0),
                    child: OutlinedButton(
                      onPressed: () {
                        setState(() {
                          choiceValuesClicked[itemIndex].value = !choiceValuesClicked[itemIndex].value!;

                          _processCheckLogic(choiceValuesClicked[itemIndex].value!, widget.choiceList[itemIndex]);
                          _processNextButtonActive(choiceValuesClicked[itemIndex].value, widget.choiceList[itemIndex]);
                        });

                        _choicesClicked() ? animationController.forward() : animationController.reverse();
                      },
                      style: TextButton.styleFrom(
                          foregroundColor: Color(0xFF00A1CD),
                          side: new BorderSide(color: choiceValuesClicked[itemIndex].value! ? Color(0xFF00A1CD) : Color(0xFFAAAAAA)),
                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0))),
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 40,
                            child: FaIcon(
                              widget.iconList[itemIndex],
                              size: 20,
                              color: choiceValuesClicked[itemIndex].value! ? Color(0xFF00A1CD) : Color(0xFFAAAAAA),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                widget.choiceList[itemIndex].choiceText,
                                style: TextStyle(
                                  fontFamily: 'Akko Pro',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: Colors.black,
                                  decoration: TextDecoration.none,
                                  decorationThickness: 0,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: OpacityButton(
                buttonText: widget.questionNextTranslationText,
                onPressedAction: _checkNextButtonPressed(),
                opacityAnimation: opacityAnimation,
              ),
            )
          ],
        ),
      ),
    );
  }
}
