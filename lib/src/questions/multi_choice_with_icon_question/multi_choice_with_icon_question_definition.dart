import 'package:flutter/material.dart';

import '../../definitions/choice_definition.dart';
import '../../definitions/question_base.dart';

class MultipleChoiceWithIconQuestionDefinition implements QuestionBase {
  // represents the question type.
  final QuestionType _type;

  // contains the question number.
  final int _questionNumber;

  // contains the question text.
  final String _questionText;

  // contains the list of choices.
  final List<ChoiceDefinition> _choiceList;

  // contains the list of icons.
  final List<IconData> _iconList;

  // optional parameter contains the hint text.
  final String? _hintText;

  @override
  final hasNextButton = true;

  @override
  QuestionType get type {
    return _type;
  }

  @override
  int get questionNumber {
    return _questionNumber;
  }

  @override
  String get questionText {
    return _questionText;
  }

  List<ChoiceDefinition> get choiceList {
    return [..._choiceList];
  }

  List<IconData> get iconList {
    return [..._iconList];
  }

  String? get hintText {
    return _hintText;
  }

  MultipleChoiceWithIconQuestionDefinition({
    type = QuestionType.multichoice_icon,
    required questionNumber,
    required questionText,
    required choiceList,
    required iconList,
    hintText,
  })  : _type = type,
        _questionNumber = questionNumber,
        _questionText = questionText,
        _choiceList = choiceList,
        _iconList = iconList,
        _hintText = hintText;
}
