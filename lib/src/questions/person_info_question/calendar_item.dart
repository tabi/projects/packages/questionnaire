import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'selected_date_mixin.dart';
import '../../mixins/animation_mixin.dart';
import '../../widgets/opacity_button.dart';

class CalendarWidget extends StatefulWidget {
  final int personNumber;
  final Color backgroundColor;
  final DateTime? dateOfBirthValue;
  final String dateOfBirthHintText;
  final String dateOfBirthDialogText;
  final String confirmButtonText;
  final String dateInThePastText;
  final SelectedDateMixin delegate;

  const CalendarWidget({
    Key? key,
    required this.personNumber,
    required this.backgroundColor,
    required this.dateOfBirthValue,
    required this.dateOfBirthHintText,
    required this.dateOfBirthDialogText,
    required this.confirmButtonText,
    required this.dateInThePastText,
    required this.delegate,
  }) : super(key: key);

  @override
  _CalendarWidgetState createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget>
    with SingleTickerProviderStateMixin, AnimationMixin {
  DateTime? _selectedDate;
  bool validDate = true;

  Function? _checkNextButtonPressed() {
    if (validDate) {
      return () {
        widget.delegate
            .onSelectedDateOfBirth(_selectedDate, widget.personNumber);
        Navigator.pop(context);
      };
    } else {
      return null;
    }
  }

  @override
  void initState() {
    super.initState();
    createAnimation(tickerprovider: this);

    // make sure the confirmation button is active!
    animationController.forward();
  }

  void show(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        builder: (context) {
          return StatefulBuilder(builder: (
            BuildContext context,
            StateSetter setModalState,
          ) {
            return SizedBox(
              height: 300,
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                    bottom: 25,
                  ),
                  child: Center(
                    child: Text(
                      widget.dateOfBirthDialogText,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
                Container(
                  height: 140,
                  child: DatePickerWidget(
                    looping: true,
                    initialDate: _selectedDate,
                    dateFormat: "dd-MMM-yyyy",
                    locale: DatePicker.localeFromString('nl'),
                    onChange: (DateTime newDate, _) {
                      setModalState(() {
                        if (newDate.compareTo(DateTime.now()) > 0) {
                          validDate = false;
                          animationController.reverse();
                        } else {
                          validDate = true;
                          animationController.forward();
                          _selectedDate = newDate;
                        }
                      });
                    },
                    pickerTheme: DateTimePickerTheme(
                      showTitle: false,
                      itemTextStyle:
                          TextStyle(color: Colors.black, fontSize: 19),
                      dividerColor: Colors.transparent,
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                ),
                Container(
                  child: Expanded(
                    child: Column(
                      children: [
                        if (!validDate)
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                widget.dateInThePastText,
                                style:
                                    TextStyle(color: Colors.red, fontSize: 16),
                              ),
                            ),
                          ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: OpacityButton(
                            buttonText: widget.confirmButtonText,
                            onPressedAction: _checkNextButtonPressed(),
                            opacityAnimation: opacityAnimation,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ]),
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    final formatter = new DateFormat('dd MMMM yyyy');
    if (_selectedDate == null) {
      _selectedDate = DateTime.now();
    }

    var val = widget.dateOfBirthValue != null
        ? formatter.format(widget.dateOfBirthValue!)
        : widget.dateOfBirthHintText;

    return Listener(
      onPointerDown: (val) {
        show(context);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: widget.backgroundColor),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              val,
              style: widget.dateOfBirthValue != null
                  ? Theme.of(context).textTheme.headline4
                  : Theme.of(context).textTheme.headline5,
            ),
            IconButton(
              icon: FaIcon(FontAwesomeIcons.calendarAlt),
              iconSize: 21,
              alignment: Alignment.centerRight,
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
