mixin SelectedDateMixin {
  void onSelectedDateOfBirth(
    DateTime? date,
    int personNumber,
  );
}