import 'dart:convert';

import 'package:intl/intl.dart';

import '../../definitions/question_result_base.dart';

// represents a person
class Person {
  final int? personNumber;
  final String? gender;
  final DateTime? dateOfBirth;
  String? relation;

  Person({
    required this.personNumber,
    required this.gender,
    required this.dateOfBirth,
    required this.relation,
  });

  @override
  String toString() {
    final formatter = new DateFormat('dd MMMM yyyy');
    var dateString = formatter.format(dateOfBirth!);
    var relationString =
        this.relation != null ? ' and relation [$relation]' : '';
    return 'Person [$personNumber], gender [$gender], dateOfBirth [$dateString]$relationString ';
  }

  factory Person.fromJson(Map<String, dynamic> data) => Person(
        personNumber: data["personNumber"],
        gender: data["gender"],
        dateOfBirth: DateTime.parse(data["dateOfBirth"]),
        relation: data["relation"],
      );

  Map<String, dynamic> toJson() {
    return {
      "personNumber": personNumber,
      "gender": gender,
      "dateOfBirth": dateOfBirth!.toIso8601String(),
      "relation": relation,
    };
  }
}

class PersonInfoResult implements QuestionResultBase {
  // represents the question number.
  final int? _questionNumber;

  // represents a list of persons
  List<Person>? _personList;

  PersonInfoResult({
    required questionNumber,
    required personList,
  })  : _questionNumber = questionNumber,
        _personList = personList;

  @override
  int? get questionNumber {
    return _questionNumber;
  }

  List<Person> get personList {
    return [..._personList!];
  }

  void setPersonList(List<Person> value) {
    _personList = value;
  }

  @override
  String toString() {
    var resultString = "";
    for (var item in personList) {
      resultString += item.toString();
    }

    return resultString;
  }

  factory PersonInfoResult.fromJson(Map<String, dynamic> data) =>
      PersonInfoResult(
          questionNumber: data["questionNumber"],
          personList: (jsonDecode(data["personList"]) as List)
              .map((i) => Person.fromJson(i))
              .toList());

  Map<String, dynamic> toJson() {
    return {
      "questionNumber": questionNumber,
      "personList": jsonEncode(personList),
    };
  }
}
