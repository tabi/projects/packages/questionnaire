import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../definitions/choice_definition.dart';
import '../single_choice_question/single_choice_question_result.dart';
import 'selected_gender_mixin.dart';

class GenderWidget extends StatefulWidget {
  final String? currentGender;
  final int personNumber;
  final List<ChoiceDefinition> choiceList;
  final Color backgroundColor;
  final String genderHintText;
  final String genderDialogText;
  final SelectedGenderMixin delegate;

  const GenderWidget({
    Key? key,
    required this.currentGender,
    required this.personNumber,
    required this.choiceList,
    required this.backgroundColor,
    required this.genderHintText,
    required this.genderDialogText,
    required this.delegate,
  }) : super(key: key);

  @override
  _GenderWidgetState createState() => _GenderWidgetState();
}

class _GenderWidgetState extends State<GenderWidget> {
  late SingleChoiceResult choiceClicked;

  @override
  void initState() {
    choiceClicked = SingleChoiceResult(questionNumber: 0, choiceNumber: 0);
    super.initState();
  }

  void show(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        builder: (context) {
          return SizedBox(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, top: 25, right: 20, bottom: 10),
                  child: Text(
                    widget.genderDialogText,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                for (var item in widget.choiceList)
                  Padding(
                    padding: const EdgeInsets.only(left: 23, right: 23),
                    child: Row(
                      mainAxisAlignment: item.addTextField
                          ? MainAxisAlignment.spaceEvenly
                          : MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Listener(
                                onPointerDown: (val) {
                                  setState(() {
                                    choiceClicked = SingleChoiceResult(
                                      questionNumber: 0,
                                      choiceNumber: item.choiceNumber,
                                    );
                                  });

                                  widget.delegate.onSelectedGender(
                                      widget.choiceList[item.choiceNumber - 1]
                                          .choiceText,
                                      widget.personNumber);
                                  Navigator.pop(context);
                                },
                                child: RadioListTile(
                                  title: Text(
                                    item.choiceText,
                                    maxLines: 2,
                                    softWrap: true,
                                  ),
                                  value: item.choiceNumber,
                                  groupValue: choiceClicked.choiceNumber,
                                  onChanged:
                                      (dynamic value) {}, // This onChange handler is taken over by the Listener !!
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 20,
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var val = widget.currentGender ?? widget.genderHintText;

    return Listener(
      onPointerDown: (val) {
        show(context);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: widget.backgroundColor),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              val,
              style: widget.currentGender != null &&
                      widget.currentGender!.isNotEmpty
                  ? Theme.of(context).textTheme.headline4
                  : Theme.of(context).textTheme.headline5,
            ),
            IconButton(
              icon: FaIcon(FontAwesomeIcons.sortDown),
              iconSize: 21,
              alignment: Alignment.centerRight,
              onPressed: () {}, // Taken over by Listener
            ),
          ],
        ),
      ),
    );
  }
}
