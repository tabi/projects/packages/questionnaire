import 'package:flutter/foundation.dart';

import '../../definitions/choice_definition.dart';
import '../../definitions/question_base.dart';

class PersonInfoQuestionDefinition implements QuestionBase {
  // represents the question type.
  final QuestionType _type;

  // contains the question number.
  final int _questionNumber;

  // contains the question text 1.
  final String _questionText;

  // contains the question text 2.
  final String _questionText2;

  // contains the question text 3.
  final String _questionText3;

  // optional parameter contains the hint text.
  final String? _hintText;

  // contains the question number which represents the number of displayed persons.
  final int _questionNumberPersons;

  // this text represents the title of every person. eg: Person.
  final String _titleText;

  // the key is the question number where the conditions should be applied to.
  // the value is a List<int> which represents the choice numbers where a relation item should be displayed.
  final Map<int, List<int>> _relationMap;

  // contains the items from the relation choice list.
  final List<ChoiceDefinition> _relationChoices;

  // contains the items from the gender choice list.
  final List<ChoiceDefinition> _genderChoices;

  @override
  bool get hasNextButton {
    return true;
  }

  @override
  QuestionType get type {
    return _type;
  }

  @override
  int get questionNumber {
    return _questionNumber;
  }

  @override
  String get questionText {
    return _questionText;
  }

  String get questionText2 {
    return _questionText2;
  }

  String get questionText3 {
    return _questionText3;
  }

  int get questionNumberPersons {
    return _questionNumberPersons;
  }

  String get titleText {
    return _titleText;
  }

  Map<int, List<int>> get relationMap {
    return {..._relationMap};
  }

  List<ChoiceDefinition> get relationChoices {
    return [..._relationChoices];
  }

  List<ChoiceDefinition> get genderChoices {
    return [..._genderChoices];
  }

  String? get hintText {
    return _hintText;
  }

  PersonInfoQuestionDefinition({
    type = QuestionType.personinfo,
    required questionNumber,
    required questionText,
    required questionText2,
    required questionText3,
    required questionNumberPersons,
    required titleText,
    required relationMap,
    required relationChoices,
    required genderChoices,
    hintText,
  })  : _type = type,
        _questionNumber = questionNumber,
        _questionText = questionText,
        _questionText2 = questionText2,
        _questionText3 = questionText3,
        _questionNumberPersons = questionNumberPersons,
        _titleText = titleText,
        _relationMap = relationMap,
        _relationChoices = relationChoices,
        _genderChoices = genderChoices,
        _hintText = hintText;
}
