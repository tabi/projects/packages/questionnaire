mixin SelectedRelationMixin {
  void onSelectedRelation(
    String relation,
    int personNumber,
  );
}