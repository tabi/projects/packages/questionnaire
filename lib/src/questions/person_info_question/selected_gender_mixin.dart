mixin SelectedGenderMixin {
  void onSelectedGender(
    String gender,
    int personNumber,
  );
}