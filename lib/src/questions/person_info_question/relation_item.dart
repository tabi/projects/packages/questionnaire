import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../definitions/choice_definition.dart';
import '../single_choice_question/single_choice_question_result.dart';
import 'selected_relation_mixin.dart';

class RelationWidget extends StatefulWidget {
  final int personNumber;
  final Color backgroundColor;
  final String? relationValue;
  final List<ChoiceDefinition> choiceList;
  final String hintText;
  final String relationDialogText;
  final SelectedRelationMixin delegate;

  const RelationWidget({
    Key? key,
    required this.personNumber,
    required this.backgroundColor,
    required this.relationValue,
    required this.choiceList,
    required this.hintText,
    required this.relationDialogText,
    required this.delegate,
  }) : super(key: key);

  @override
  _RelationWidgetState createState() => _RelationWidgetState();
}

class _RelationWidgetState extends State<RelationWidget> {
  late SingleChoiceResult choiceClicked;

  @override
  void initState() {
    choiceClicked = SingleChoiceResult(questionNumber: 0, choiceNumber: 0);
    super.initState();
  }

  void show(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        builder: (context) {
          return SizedBox(
            height: 350,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, top: 25, right: 20, bottom: 10),
                  child: Text(
                    widget.relationDialogText,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                for (var item in widget.choiceList)
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 23, right: 23),
                      child: Row(
                        mainAxisAlignment: item.addTextField
                            ? MainAxisAlignment.spaceEvenly
                            : MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Listener(
                                  onPointerDown: (val) {
                                    setState(() {
                                      choiceClicked = SingleChoiceResult(
                                        questionNumber: 0,
                                        choiceNumber: item.choiceNumber,
                                      );
                                    });

                                    widget.delegate.onSelectedRelation(
                                        widget.choiceList[item.choiceNumber - 1]
                                            .choiceText,
                                        widget.personNumber);
                                    Navigator.pop(context);
                                  },
                                  child: SizedBox(
                                    height: 30,
                                    child: RadioListTile(
                                        title: Text(
                                          item.choiceText,
                                          maxLines: 2,
                                          softWrap: true,
                                        ),
                                        value: item.choiceNumber,
                                        groupValue: choiceClicked.choiceNumber,
                                        onChanged:
                                            (dynamic value) {} // This onChange handler is taken over by the Listener !!
                                        ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                Container(
                  height: 20,
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var val = widget.relationValue ?? widget.hintText;

    return Listener(
      onPointerDown: (val) {
        show(context);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: widget.backgroundColor),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              val,
              style: widget.relationValue != null &&
                      widget.relationValue!.isNotEmpty
                  ? Theme.of(context).textTheme.headline4
                  : Theme.of(context).textTheme.headline5,
            ),
            IconButton(
              icon: FaIcon(FontAwesomeIcons.userFriends),
              iconSize: 21,
              alignment: Alignment.centerRight,
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
