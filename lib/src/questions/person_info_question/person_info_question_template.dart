import 'dart:math';

import 'package:flutter/material.dart';

import '../../definitions/choice_definition.dart';
import '../../mixins/animation_mixin.dart';
import '../../widgets/opacity_button.dart';
import 'calendar_item.dart';
import 'gender_item.dart';
import 'person_info_question_result.dart';
import 'relation_item.dart';
import 'selected_date_mixin.dart';
import 'selected_gender_mixin.dart';
import 'selected_relation_mixin.dart';

class PersonInfoChoiceQuestion extends StatefulWidget {
  final List<Person>? value;
  final ValueChanged<PersonInfoResult?> questionAnswered;
  final String questionTranslationText;
  final String questionNextTranslationText;
  final String questionText;
  final String? hintText;
  final int questionNumber;
  final int numberInList;
  final bool hasNextButton;
  final int inputDataNumberOfPersons;
  final String titleText;
  final Color backgroundColor;
  final Map<int, List<int>> relationMap;
  final List<ChoiceDefinition> relationChoices;
  final List<ChoiceDefinition> genderChoices;
  final String yourselfText;
  final String genderHintText;
  final String genderDialogText;
  final String dateOfBirthHintText;
  final String dateOfBirthDialogText;
  final String relationHintText;
  final String relationText;
  final String confirmButtonText;
  final String dateInThePastText;
  final bool showRelation;

  PersonInfoChoiceQuestion({
    Key? key,
    required this.questionNumber,
    required this.questionText,
    required this.questionTranslationText,
    required this.questionNextTranslationText,
    required this.questionAnswered,
    required this.hasNextButton,
    required this.inputDataNumberOfPersons,
    required this.titleText,
    required this.backgroundColor,
    required this.relationMap,
    required this.relationChoices,
    required this.yourselfText,
    required this.genderChoices,
    required this.genderHintText,
    required this.genderDialogText,
    required this.dateOfBirthHintText,
    required this.dateOfBirthDialogText,
    required this.relationHintText,
    required this.relationText,
    required this.confirmButtonText,
    required this.dateInThePastText,
    required this.showRelation,
    required this.numberInList,
    this.hintText,
    this.value,
  }) : super(key: key);

  @override
  _PersonInfoChoiceQuestionState createState() => _PersonInfoChoiceQuestionState();
}

class _PersonInfoChoiceQuestionState extends State<PersonInfoChoiceQuestion>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin, SelectedGenderMixin, SelectedDateMixin, SelectedRelationMixin, AnimationMixin {
  PersonInfoResult? personInfoResult;
  List<ChoiceDefinition>? choiceList;
  var currentGender = List<String?>.empty(growable: true);
  var currentDateOfBirth = List<DateTime?>.empty(growable: true);
  var currentRelation = List<String?>.empty(growable: true);
  int? numberOfPersons;
  final scrollController = ScrollController();

  void _animate() {
    _allFieldsFilled() ? animationController.forward() : animationController.reverse();
  }

  // check if all the fields are filled in.
  bool _allFieldsFilled() {
    for (var cnt = 0; cnt < numberOfPersons!; cnt++) {
      if (currentGender[cnt] == null || currentDateOfBirth[cnt] == null) {
        return false;
      }

      if (widget.showRelation && currentRelation[cnt] == null && cnt != 0) {
        return false;
      }
    }

    return true;
  }

  void _initLists(List<Person>? storedPersons) {
    if (storedPersons != null && storedPersons.length > 0) {
      storedPersons.forEach((element) {
        currentGender.add(element.gender);
        currentDateOfBirth.add(element.dateOfBirth);
        currentRelation.add(element.relation);
      });
    } else {
      for (var cnt = 0; cnt != numberOfPersons; cnt++) {
        currentGender.add(null);
        currentDateOfBirth.add(null);
        currentRelation.add(null);
      }
    }
  }

  void _collectAnswerData() {
    var personList = List<Person>.empty(growable: true);
    for (int cnt = 0; cnt != numberOfPersons; cnt++) {
      var person = Person(personNumber: cnt + 1, gender: currentGender[cnt], dateOfBirth: currentDateOfBirth[cnt], relation: currentRelation[cnt]);
      personList.add(person);
    }

    personInfoResult!.setPersonList(personList);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    createAnimation(tickerprovider: this);

    numberOfPersons = min(widget.inputDataNumberOfPersons, 8);

    _initLists(widget.value);

    personInfoResult = PersonInfoResult(questionNumber: widget.questionNumber, personList: null);

    _animate();
  }

  @override
  void onSelectedGender(String gender, int personNumber) {
    setState(() {
      currentGender[personNumber - 1] = gender;
    });

    _animate();
  }

  @override
  void onSelectedDateOfBirth(DateTime? date, int personNumber) {
    setState(() {
      currentDateOfBirth[personNumber - 1] = date;
    });

    _animate();
  }

  @override
  void onSelectedRelation(String relation, int personNumber) {
    setState(() {
      currentRelation[personNumber - 1] = relation;
    });

    _animate();
  }

  @override
  void dispose() {
    currentGender?.clear();
    currentDateOfBirth?.clear();
    currentRelation?.clear();

    choiceList?.clear();

    super.dispose();
  }

  Function? _checkNextButtonPressed() {
    if (_allFieldsFilled()) {
      return () {
        FocusScope.of(context).unfocus();
        _collectAnswerData();
        widget.questionAnswered(personInfoResult);
      };
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    // extend the lists if more persons are added.
    if (numberOfPersons! < widget.inputDataNumberOfPersons) {
      int numberOfPersonsToAdd = widget.inputDataNumberOfPersons - numberOfPersons!;
      for (var cnt = 0; cnt != numberOfPersonsToAdd; cnt++) {
        currentGender.add(null);
        currentDateOfBirth.add(null);
        currentRelation.add(null);
      }
    }

    // restrict the number of persons to max 8
    numberOfPersons = min(widget.inputDataNumberOfPersons, 8);

    return RawScrollbar(
      controller: scrollController,
      thumbColor: Colors.grey,
      thickness: 8,
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 19),
              child: Text(
                '${widget.questionTranslationText} ${widget.numberInList}'.toUpperCase(),
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 19, top: 12, right: 17),
              child: Text(
                widget.questionText,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            (widget.hintText != null)
                ? Padding(
                    padding: const EdgeInsets.only(left: 19, top: 12, right: 17),
                    child: Text(
                      widget.hintText!,
                      style: Theme.of(context).textTheme.caption!.copyWith(fontStyle: FontStyle.italic),
                    ),
                  )
                : Container(),
            for (var persons = 1; persons <= numberOfPersons!; persons++)
              Padding(
                padding: const EdgeInsets.only(left: 19, top: 15, right: 17),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      persons == 1 ? '${widget.titleText} $persons. (${widget.yourselfText})' : '${widget.titleText} $persons.',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    GenderWidget(
                      currentGender: currentGender[persons - 1],
                      personNumber: persons,
                      choiceList: widget.genderChoices,
                      backgroundColor: widget.backgroundColor,
                      genderHintText: widget.genderHintText,
                      genderDialogText: widget.genderDialogText,
                      delegate: this,
                    ),
                    CalendarWidget(
                      personNumber: persons,
                      backgroundColor: widget.backgroundColor,
                      dateOfBirthValue: currentDateOfBirth[persons - 1],
                      dateOfBirthHintText: widget.dateOfBirthHintText,
                      dateOfBirthDialogText: widget.dateOfBirthDialogText,
                      confirmButtonText: widget.confirmButtonText,
                      dateInThePastText: widget.dateInThePastText,
                      delegate: this,
                    ),
                    if (widget.showRelation != null && widget.showRelation && persons != 1)
                      RelationWidget(
                        personNumber: persons,
                        choiceList: widget.relationChoices,
                        backgroundColor: widget.backgroundColor,
                        hintText: widget.relationHintText,
                        relationDialogText: widget.relationText,
                        relationValue: currentRelation[persons - 1],
                        delegate: this,
                      ),
                  ],
                ),
              ),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: OpacityButton(
                buttonText: widget.questionNextTranslationText,
                onPressedAction: _checkNextButtonPressed(),
                opacityAnimation: opacityAnimation,
              ),
            )
          ],
        ),
      ),
    );
  }
}
