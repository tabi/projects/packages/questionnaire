
import '../../definitions/question_base.dart';
import '../multi_choice_question/multi_choice_question_definition.dart';

class PersonEducationChoiceQuestion extends MultipleChoiceQuestionDefinition {
  // contains the question number which represents the persons where to fetch the data from.
  final int _questionNumberPersons;

  int get questionNumberPersons {
    return _questionNumberPersons;
  }

  PersonEducationChoiceQuestion({
    required questionNumber,
    required questionText,
    required questionNumberPersons,
    hintText,
  })  : _questionNumberPersons = questionNumberPersons,
        super(
            type: QuestionType.personeducation,
            questionNumber: questionNumber,
            questionText: questionText,
            choiceList: null,
            hintText: hintText);
}
