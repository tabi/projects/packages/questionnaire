import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../definitions/choice_definition.dart';
import '../../mixins/animation_mixin.dart';
import '../../mixins/focus_mixin.dart';
import '../../widgets/always_disabled_focusnode.dart';
import 'multi_choice_question_result.dart';

class MultipleChoiceQuestion extends StatefulWidget {
  final List<MultiChoiceResultItem>? value;
  final ValueChanged<MultiChoiceResult?> questionAnswered;
  final ValueChanged<MultiChoiceResult?> questionReturned;
  final String questionTranslationText;
  final String questionNextTranslationText;
  final String questionText;
  final String? hintText;
  final List<ChoiceDefinition> choiceList;
  final int questionNumber;
  final int numberInList;

  MultipleChoiceQuestion({
    Key? key,
    required this.questionNumber,
    required this.choiceList,
    required this.questionText,
    required this.questionTranslationText,
    required this.questionNextTranslationText,
    required this.questionAnswered,
    required this.questionReturned,
    required this.numberInList,
    this.hintText,
    this.value,
  }) : super(key: key);

  @override
  _MultipleChoiceQuestionState createState() => _MultipleChoiceQuestionState();
}

class _MultipleChoiceQuestionState extends State<MultipleChoiceQuestion>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin, AnimationMixin, FocusAndTextControllerMixin {
  MultiChoiceResult? multiChoiceResult;
  List<MultiChoiceResultItem> choiceValuesClicked = List<MultiChoiceResultItem>.empty(growable: true);
  AlwaysDisabledFocusNode? alwaysDisabledFocusNode;

  // used mainly for layout adjustments
  late bool hasNoTextfields;

  late bool nextButtonActive;
  late bool previousButtonActive;

  final scrollController = ScrollController();

  List<MultiChoiceResultItem> _createChoiceValues(List<MultiChoiceResultItem>? choicesClicked) {
    for (var choice in widget.choiceList) {
      choiceValuesClicked.add(MultiChoiceResultItem(choiceNumber: choice.choiceNumber, value: false, textField: null));
    }

    if (choicesClicked != null && choicesClicked.length > 0) {
      choicesClicked.forEach((element) {
        var idx = choiceValuesClicked.indexWhere((el) => el.choiceNumber == element.choiceNumber);
        if (idx != -1) {
          choiceValuesClicked[idx].value = true;
          choiceValuesClicked[idx].textField = element.textField;
        }
      });
    }

    return choiceValuesClicked;
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    hasNoTextfields = true;
    nextButtonActive = false;
    previousButtonActive = true;
    createAnimation(tickerprovider: this);
    createFocusAndTextControllers(length: widget.choiceList.length);
    alwaysDisabledFocusNode = AlwaysDisabledFocusNode();

    choiceValuesClicked = _createChoiceValues(widget.value);

    nextButtonActive = _checkIfAllTextFieldsAreFilled();
    if (nextButtonActive) {
      animationController.forward();
    }

    for (var choice in widget.choiceList) {
      textEditingControllers[choice.choiceNumber - 1] = TextEditingController();
      textFocusNodes[choice.choiceNumber - 1] = FocusNode();

      if (choice.addTextField) {
        hasNoTextfields = false;
      }
    }

    multiChoiceResult = MultiChoiceResult(questionNumber: widget.questionNumber, resultList: null);

    // fill the textfields if there are out of the stored answers.
    choiceValuesClicked.forEach((element) {
      if (element.textField != null && element.textField!.isNotEmpty) {
        textEditingControllers[element.choiceNumber! - 1]!.text = element.textField!;
      }
    });
  }

  @override
  void dispose() {
    disposeFocusAndTextControllers(length: widget.choiceList.length);

    choiceValuesClicked.clear();

    super.dispose();
  }

  bool _choicesClicked() {
    bool result = true;
    if (choiceValuesClicked != null) {
      var clickedChoices = choiceValuesClicked.where((choice) => choice.value == true).toList();

      if (clickedChoices.length == 0) {
        result = false;
      } else {
        clickedChoices.forEach((element) {
          if (element.textField != null && element.textField!.isEmpty) {
            result = false;
          }
        });
      }
    } else {
      result = false;
    }

    return result;
  }

  void _resetTextField(int choiceNumber) {
    textEditingControllers[choiceNumber - 1]!.clear();
  }

  void _resetChoicesClicked() {
    for (var choice in widget.choiceList) {
      choiceValuesClicked.add(MultiChoiceResultItem(choiceNumber: choice.choiceNumber, value: false, textField: null));
      textEditingControllers[choice.choiceNumber - 1]!.clear();
    }
  }

  void _clearAllChoices() {
    choiceValuesClicked.clear();
    _resetChoicesClicked();
  }

  void _clearResetFields() {
    for (var choice in widget.choiceList) {
      if (choice.resetOtherFields ?? false) {
        choiceValuesClicked[choice.choiceNumber - 1].value = false;
        textEditingControllers[choice.choiceNumber - 1]!.clear();
      }
    }
  }

  Function? _checkNextButtonPressed() {
    if (nextButtonActive) {
      FocusScope.of(context).unfocus();
      widget.questionAnswered(_collectAnswerData());
      animationController.forward();
      return null;
    } else {
      return null;
    }
  }

  Function? _checkPreviousButtonPressed() {
    if (previousButtonActive) {
      FocusScope.of(context).unfocus();
      widget.questionAnswered(_collectAnswerData());
      animationController.forward();

      return null;
    } else {
      return null;
    }
  }

  void _resetFocusOnTextFields() {
    for (int c = 0; c != textFocusNodes.length; c++) {
      textFocusNodes[c]?.unfocus();
    }
  }

  MultiChoiceResult? _collectAnswerData() {
    multiChoiceResult!.setResultList(choiceValuesClicked.where((item) => item.value == true).toList());
    return multiChoiceResult;
  }

  bool _checkIfAllTextFieldsAreFilled() {
    bool result = true;
    var activeChoices = choiceValuesClicked.where((choice) => choice.value == true);

    if (activeChoices.length == 0) {
      result = false;
    }

    activeChoices.forEach((element) {
      if (element.textField != null && element.textField!.isEmpty) {
        result = false;
      }
    });

    return result;
  }

  bool _processCheckLogic(bool val, ChoiceDefinition item) {
    if (val) {
      item.resetOtherFields ?? false ? _clearAllChoices() : _clearResetFields();
    } else {
      if (item.addTextField) {
        _resetTextField(item.choiceNumber);
      }
    }

    return true;
  }

  void _processFocusAndUnfocus(ChoiceDefinition item, bool? val) {
    if (item.addTextField && val!) {
      textFocusNodes[item.choiceNumber - 1]!.requestFocus();

      textEditingControllers[item.choiceNumber - 1]!.selection =
          TextSelection(baseOffset: 0, extentOffset: textEditingControllers[item.choiceNumber - 1]!.text.length);
    } else {
      _resetFocusOnTextFields();
    }
  }

  void _processNextButtonActive(bool? val, ChoiceDefinition item) {
    setState(() {
      if ((val! && !item.addTextField) || (val && (item.addTextField && textEditingControllers[item.choiceNumber - 1]!.text.isNotEmpty))) {
        nextButtonActive = true;
      } else {
        nextButtonActive = _checkIfAllTextFieldsAreFilled();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var choiceCount = 0;

    return Column(
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(bottom: 12),
            padding: const EdgeInsets.symmetric(vertical: 17),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: RawScrollbar(
              controller: scrollController,
              thumbColor: Colors.grey,
              thickness: 8,
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: Text(
                        '${widget.questionTranslationText} ${widget.numberInList}'.toUpperCase(),
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
                      child: Text(
                        widget.questionText,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    (widget.hintText != null)
                        ? Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
                            child: Text(widget.hintText!, style: Theme.of(context).textTheme.caption),
                          )
                        : Container(),
                    for (var item in widget.choiceList)
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0, right: 20.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: hasNoTextfields ? 1 : 4,
                              child: Column(
                                children: [
                                  CheckboxListTile(
                                    title: Text(
                                      item.choiceText,
                                      maxLines: 2,
                                      softWrap: true,
                                      style: Theme.of(context).textTheme.headline4,
                                    ),
                                    controlAffinity: ListTileControlAffinity.leading,
                                    value: choiceValuesClicked[choiceCount++].value,
                                    onChanged: (val) {
                                      setState(() {
                                        if (!_processCheckLogic(val!, item)) {
                                          choiceValuesClicked[item.choiceNumber - 1] = MultiChoiceResultItem(
                                            choiceNumber: item.choiceNumber,
                                            value: val,
                                            textField: item.addTextField ? textEditingControllers[item.choiceNumber - 1]!.text : null,
                                          );
                                          _processNextButtonActive(val, item);
                                          return;
                                        }

                                        // add clicked item
                                        choiceValuesClicked[item.choiceNumber - 1] = MultiChoiceResultItem(
                                          choiceNumber: item.choiceNumber,
                                          value: val,
                                          textField: item.addTextField ? textEditingControllers[item.choiceNumber - 1]!.text : null,
                                        );

                                        _processFocusAndUnfocus(item, val);
                                        _processNextButtonActive(val, item);
                                      });

                                      _choicesClicked() ? animationController.forward() : animationController.reverse();
                                    },
                                  ),
                                ],
                              ),
                            ),
                            if (!hasNoTextfields)
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    item.addTextField
                                        ? TextFormField(
                                            style: choiceValuesClicked[item.choiceNumber - 1].value!
                                                ? Theme.of(context).textTheme.headline4
                                                : Theme.of(context).textTheme.headline5,
                                            controller: textEditingControllers[item.choiceNumber - 1],
                                            focusNode: choiceValuesClicked[item.choiceNumber - 1].value!
                                                ? textFocusNodes[item.choiceNumber - 1]
                                                : alwaysDisabledFocusNode,
                                            keyboardType: TextInputType.number,
                                            enabled: choiceValuesClicked[item.choiceNumber - 1].value,
                                            decoration: InputDecoration(hintText: item.textFieldHint),
                                            onChanged: (value) {
                                              if (value.isNotEmpty) {
                                                choiceValuesClicked[item.choiceNumber - 1].textField = value;
                                                textFocusNodes[item.choiceNumber - 1]!.unfocus();
                                                setState(() {
                                                  nextButtonActive = true;
                                                });
                                              } else {
                                                choiceValuesClicked[item.choiceNumber - 1].textField = value;
                                                setState(() {
                                                  nextButtonActive = false;
                                                });
                                              }

                                              _choicesClicked() ? animationController.forward() : animationController.reverse();
                                            },
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: _checkPreviousButtonPressed,
                child: Text('Vorige'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: ColorReel.lightGreen,
                  textStyle: buttonText18,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton(
                onPressed: _checkNextButtonPressed,
                child: Text('Volgende'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: nextButtonActive ? ColorReel.lightGreen : ColorReel.lightGray,
                  textStyle: buttonText18,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 12,
        ),
      ],
    );
  }
}
