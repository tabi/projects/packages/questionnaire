import 'dart:convert';

import '../../definitions/question_result_base.dart';

// contains the answer from a multiple choice question.
class MultiChoiceResultItem {
  // represents the choice number.
  final int? choiceNumber;

  // represents the choice value. false = not checked, true = checked.
  bool? value;

  // represents the optional textfield.
  String? textField;

  MultiChoiceResultItem({
    required this.choiceNumber,
    required this.value,
    this.textField,
  });

  @override
  String toString() {
    var textString =
        this.textField != null ? ' and textfield [$textField]' : '';
    return 'Choice [$choiceNumber], value [$value]$textString ';
  }

  factory MultiChoiceResultItem.fromJson(Map<String, dynamic> data) =>
      MultiChoiceResultItem(
        choiceNumber: data["choiceNumber"],
        value: data["value"],
        textField: data["textField"],
      );

  Map<String, dynamic> toJson() {
    return {
      "choiceNumber": choiceNumber,
      "value": value,
      "textField": textField,
    };
  }
}

class MultiChoiceResult implements QuestionResultBase {
  // represents the question number.
  final int? _questionNumber;

  // represents a list of selected choices
  List<MultiChoiceResultItem>? _resultList;

  MultiChoiceResult({
    required questionNumber,
    required resultList,
  })  : _questionNumber = questionNumber,
        _resultList = resultList;

  @override
  int? get questionNumber {
    return _questionNumber;
  }

  List<MultiChoiceResultItem> get resultList {
    return [..._resultList!];
  }

  void setResultList(List<MultiChoiceResultItem> value) {
    _resultList = value;
  }

  @override
  String toString() {
    var resultString = "";
    for (var item in resultList) {
      resultString += item.toString();
    }

    return resultString;
  }

  factory MultiChoiceResult.fromJson(Map<String, dynamic> data) =>
      MultiChoiceResult(
          questionNumber: data["questionNumber"],
          resultList: (jsonDecode(data["resultList"]) as List)
              .map((i) => MultiChoiceResultItem.fromJson(i))
              .toList());

  Map<String, dynamic> toJson() {
    return {
      "questionNumber": questionNumber,
      "resultList": jsonEncode(resultList),
    };
  }
}
