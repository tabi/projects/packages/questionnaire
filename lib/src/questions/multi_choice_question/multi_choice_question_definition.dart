import 'package:flutter/foundation.dart';

import '../../definitions/choice_definition.dart';
import '../../definitions/question_base.dart';

class MultipleChoiceQuestionDefinition implements QuestionBase {
  // represents the question type.
  final QuestionType _type;

  // contains the question number.
  final int _questionNumber;

  // contains the question text.
  final String _questionText;

  // contains the list of choices.
  final List<ChoiceDefinition>? _choiceList;

  // optional parameter contains the hint text.
  final String? _hintText;

  @override
  final hasNextButton = true;

  @override
  QuestionType get type {
    return _type;
  }

  @override
  int get questionNumber {
    return _questionNumber;
  }

  @override
  String get questionText {
    return _questionText;
  }

  List<ChoiceDefinition> get choiceList {
    return [..._choiceList!];
  }

  String? get hintText {
    return _hintText;
  }

  MultipleChoiceQuestionDefinition({
    type = QuestionType.multichoice,
    required questionNumber,
    required questionText,
    required choiceList,
    hintText,
  })  : _type = type,
        _questionNumber = questionNumber,
        _questionText = questionText,
        _choiceList = choiceList,
        _hintText = hintText;
}
