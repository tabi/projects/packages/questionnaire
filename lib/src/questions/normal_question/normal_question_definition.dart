import 'package:flutter/widgets.dart';

import '../../definitions/choice_definition.dart';
import '../../definitions/question_base.dart';

class NormalQuestionDefinition implements QuestionBase {
  // represents the question type.
  final QuestionType _type;

  final QuestionType? _subType;

  // contains the question number.
  final int _questionNumber;

  // contains the question text.
  final String _questionText;

  // contains a map where key = result of the text and value is the questionnumber where to go to.
  final Map<String, int>? _conditions;

  // contains the hint text.
  final String _hintTextfieldText;

  // contains the hint text.
  final String? _hintTitleText;

  // contains the keyboard type for this text field.
  final TextInputType _keyboardType;

  // contains the validation text for this text field. eg: Please enter a number.
  final String? _numberValidationText;

  final String? _textFieldHeader;

  final Uri? _uriLink;

  final String? _tokenHeader;

  final Icon? _textFieldIcon;

  // contains the extra choices.
  final ChoiceDefinition? _choice;

  @override
  final hasNextButton = true;

  @override
  QuestionType get type {
    return _type;
  }

  @override
  QuestionType? get subType {
    return _subType;
  }

  @override
  int get questionNumber {
    return _questionNumber;
  }

  @override
  String get questionText {
    return _questionText;
  }

  Map<String, int> get conditions {
    return {...?_conditions};
  }

  String get hintTextfieldText {
    return _hintTextfieldText;
  }

  String? get hintTitleText {
    return _hintTitleText;
  }

  String? get textFieldHeader {
    return _textFieldHeader;
  }

  Icon? get textFieldIcon {
    return _textFieldIcon;
  }

  TextInputType get keyboardType {
    return _keyboardType;
  }

  String? get numberValidationText {
    return _numberValidationText;
  }

  String? get textfieldHeader {
    return _numberValidationText;
  }

  ChoiceDefinition? get choice {
    return _choice;
  }

  Uri? get uriLink {
    return _uriLink;
  }

  String? get tokenHeader {
    return _tokenHeader;
  }

  NormalQuestionDefinition({
    type = QuestionType.normal,
    required questionNumber,
    required questionText,
    required hintTextfieldText,
    required keyboardType,
    hintTitleText,
    numberValidationText,
    conditions,
    textFieldHeader,
    textFieldIcon,
    choice,
    uriLink,
    tokenHeader,
    subType,
  })  : _type = type,
        _questionNumber = questionNumber,
        _questionText = questionText,
        _hintTextfieldText = hintTextfieldText,
        _hintTitleText = hintTitleText,
        _keyboardType = keyboardType,
        _numberValidationText = numberValidationText,
        _conditions = conditions,
        _choice = choice,
        _textFieldHeader = textFieldHeader,
        _textFieldIcon = textFieldIcon,
        _uriLink = uriLink,
        _tokenHeader = tokenHeader,
        _subType = subType;
}
