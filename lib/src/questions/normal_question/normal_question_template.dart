import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import '../../../constants.dart';
import '../../../questionnaire.dart';
import '../../definitions/choice_definition.dart';
import '../../mixins/animation_mixin.dart';
import 'normal_question_result.dart';

class NormalQuestion extends StatefulWidget {
  final NormalResult? value;
  final String? textFieldHeader;
  final ValueChanged<NormalResult> questionAnswered;
  final ValueChanged<NormalResult> questionReturned;
  final bool resumeActive;
  final String questionTranslationText;
  final String questionNextTranslationText;
  final String questionText;
  final Map<String, int>? conditions;
  final String hintTextfieldText;
  final String? hintTitleText;
  final int questionNumber;
  final int numberInList;
  final bool hasNextButton;
  final ChoiceDefinition? choice;
  final TextInputType keyboardType;
  final Icon? textFieldIcon;
  final Uri? uriLink;
  final String? tokenHeader;
  final QuestionType? subType;

  NormalQuestion({
    Key? key,
    required this.questionNumber,
    required this.questionText,
    required this.hintTextfieldText,
    required this.hintTitleText,
    required this.questionTranslationText,
    required this.questionNextTranslationText,
    required this.questionAnswered,
    required this.questionReturned,
    required this.hasNextButton,
    required this.keyboardType,
    required this.numberInList,
    required this.resumeActive,
    required this.textFieldHeader,
    this.uriLink,
    this.tokenHeader,
    this.conditions,
    this.value,
    this.choice,
    this.textFieldIcon,
    this.subType,
  }) : super(key: key);

  @override
  _NormalQuestionState createState() => _NormalQuestionState();
}

class _NormalQuestionState extends State<NormalQuestion> with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin, AnimationMixin {
  final _textEditingController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late bool nextButtonActive;
  late bool previousButtonActive;
  bool isTicked = false;
  bool textFieldEnabled = true;

  String searchQuery = '';
  Timer? _debounce;
  late http.Response response;
  String displayAddress = '';

  @override
  void initState() {
    super.initState();

    nextButtonActive = false;
    previousButtonActive = true;

    createAnimation(tickerprovider: this);
    _textEditingController.text = widget.value?.answer ?? '';
    if (_textEditingController.text.isNotEmpty) {
      animationController.forward();
    }

    setState(() {
      nextButtonActive = false;
      previousButtonActive = true;
    });

    if (_textEditingController.text.isNotEmpty) {
      setState(() {
        nextButtonActive = true;
        previousButtonActive = true;
      });
    }
  }

  Function? _checkNextButtonPressed() {
    if (nextButtonActive) {
      FocusScope.of(context).unfocus();
      if (_textEditingController.text.isNotEmpty) {
        if (widget.subType != QuestionType.address) {
          widget.questionAnswered(NormalResult(questionNumber: widget.questionNumber, answer: _textEditingController.text));
        } else {
          widget.questionAnswered(NormalResult(questionNumber: widget.questionNumber, answer: _textEditingController.text + ' - ' + displayAddress));
        }
      } else {
        if (isTicked && widget.choice != null) {
          widget.questionAnswered(NormalResult(questionNumber: widget.questionNumber, answer: widget.choice!.choiceText));
        }
      }
      animationController.forward();
      return null;
    } else {
      return null;
    }
  }

  Function? _checkPreviousButtonPressed() {
    if (previousButtonActive) {
      FocusScope.of(context).unfocus();
      if (_textEditingController.text.isNotEmpty) {
        if (widget.subType != QuestionType.address) {
          widget.questionReturned(NormalResult(questionNumber: widget.questionNumber, answer: _textEditingController.text));
        } else {
          widget.questionReturned(NormalResult(questionNumber: widget.questionNumber, answer: _textEditingController.text + ' - ' + displayAddress));
        }
      } else if (isTicked && widget.choice != null) {
        widget.questionReturned(NormalResult(questionNumber: widget.questionNumber, answer: widget.choice!.choiceText));
      } else {
        widget.questionReturned(NormalResult(questionNumber: widget.questionNumber, answer: ''));
      }
      animationController.forward();

      return null;
    } else {
      return null;
    }
  }

  List<TextInputFormatter>? _getInputFormatter() {
    if (widget.keyboardType == TextInputType.number) {
      return <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly,
        new LengthLimitingTextInputFormatter(2),
      ];
    } else if (widget.subType == QuestionType.numberplate) {
      return <TextInputFormatter>[new LengthLimitingTextInputFormatter(9)];
    } else {
      return null;
    }
  }

  void _updateSearchQuery(String val) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    if (isValid(val)) {
      _debounce = Timer(const Duration(seconds: 2), () async {
        if (widget.uriLink != null && widget.tokenHeader != null) {
          response = await http
              .get(Uri.parse('${widget.uriLink!}$val'), headers: {'Content-Type': 'application/json', HttpHeaders.authorizationHeader: widget.tokenHeader!});
          var decoded = jsonDecode(response.body);
          if (decoded['Body'] != null) {
            try {
              var decodedBody = jsonDecode(decoded['Body'].toString().substring(1, decoded['Body'].toString().length - 1));
              setState(() {
                displayAddress = decodedBody['address'];
              });
            } catch (e) {
              setState(() {
                displayAddress = 'error';
              });
            }
          }
        }
      });
    } else {
      setState(() {
        displayAddress = '';
      });
    }
  }

  bool isValid(String s) {
    RegExp a = RegExp(r'[a-zA-Z\s]+');
    RegExp n = RegExp(r'[0-9]+');
    return a.hasMatch(s) && n.hasMatch(s);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Column(
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(bottom: 12),
            padding: const EdgeInsets.symmetric(vertical: 17),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${widget.questionTranslationText} ${widget.numberInList}'.toUpperCase(),
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12, bottom: 15),
                        child: Text(widget.questionText, style: Theme.of(context).textTheme.headline6),
                      ),
                      (widget.hintTitleText != null)
                          ? Padding(
                              padding: const EdgeInsets.only(top: 12, right: 20, bottom: 15),
                              child: Text(
                                widget.hintTitleText!,
                                style: Theme.of(context).textTheme.caption,
                                textAlign: TextAlign.start,
                              ),
                            )
                          : Container(),
                      Text(widget.textFieldHeader != null ? widget.textFieldHeader! : '', style: questionSubBaseText),
                      TextFormField(
                        enabled: textFieldEnabled,
                        controller: _textEditingController,
                        keyboardType: widget.keyboardType,
                        cursorColor: ColorReel.darkBlueish,
                        decoration: InputDecoration(
                          hintText: widget.hintTextfieldText,
                          hintStyle: hintText,
                          prefixIcon: widget.textFieldIcon != null ? widget.textFieldIcon! : null,
                        ),
                        autofocus: (widget.subType == QuestionType.birthday) ? false : !widget.resumeActive,
                        inputFormatters: _getInputFormatter(),
                        onTap: () async {
                          if (widget.keyboardType == TextInputType.datetime) {
                            DateTime? date = DateTime.now();
                            FocusScope.of(context).requestFocus(new FocusNode());

                            date = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(1900),
                                lastDate: DateTime.now(),
                                initialEntryMode: DatePickerEntryMode.inputOnly,
                                fieldLabelText: 'Geboortedatum',
                                fieldHintText: 'DD-MM-JJJJ',
                                cancelText: 'Annuleer',
                                confirmText: 'Ok',
                                builder: (context, child) {
                                  return Theme(
                                    data: Theme.of(context).copyWith(
                                        colorScheme: ColorScheme.light(
                                      primary: ColorReel.lightBlueWithOpacity,
                                    )),
                                    child: child!,
                                  );
                                });

                            if (date != null) {
                              _textEditingController.text = date.day.toString() + '-' + date.month.toString() + '-' + date.year.toString();
                              setState(() {
                                if (_textEditingController.text == '') {
                                  nextButtonActive = false;
                                } else {
                                  nextButtonActive = true;
                                }
                              });
                            }
                          }
                        },
                        onChanged: (value) {
                          if (widget.subType == QuestionType.address) {
                            _updateSearchQuery(value);
                          }
                          setState(() {
                            if (_textEditingController.text == '') {
                              nextButtonActive = false;
                            } else {
                              nextButtonActive = true;
                            }
                          });
                        },
                      ),
                      (widget.choice != null)
                          ? CheckboxListTile(
                              title: Text(
                                widget.choice!.choiceText,
                                maxLines: 2,
                                softWrap: true,
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              controlAffinity: ListTileControlAffinity.leading,
                              value: isTicked,
                              contentPadding: EdgeInsets.zero,
                              onChanged: (val) {
                                setState(() {
                                  isTicked = val!;
                                  textFieldEnabled = !val;
                                  if (isTicked) {
                                    nextButtonActive = true;
                                    _textEditingController.text = '';
                                  } else {
                                    if (_textEditingController.text.isNotEmpty) {
                                      nextButtonActive = true;
                                    } else {
                                      nextButtonActive = false;
                                    }
                                  }
                                });
                              })
                          : Container(),
                      (displayAddress != '')
                          ? Padding(
                              padding: const EdgeInsets.only(top: 30, bottom: 30),
                              child: Text('Uw adres', style: questionSubBaseText),
                            )
                          : Container(),
                      (displayAddress != '')
                          ? (displayAddress != 'error')
                              ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(color: Color(0xFFEBEEF0), borderRadius: BorderRadius.circular(5)),
                                  child: Column(children: [
                                    Padding(
                                      padding: const EdgeInsets.all(16.0),
                                      child: Text(
                                        displayAddress,
                                        style: optionText,
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                  ]),
                                )
                              : Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFE94C0A),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Container(
                                      decoration: BoxDecoration(color: Color(0xFFFFF5F1), borderRadius: BorderRadius.circular(5)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(16.0),
                                        child: Text(
                                          'Adres niet gevonden...',
                                          style: errorText16,
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: _checkPreviousButtonPressed,
                child: Text('Vorige'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: ColorReel.lightGreen,
                  textStyle: buttonText18,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton(
                onPressed: _checkNextButtonPressed,
                child: Text('Volgende'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: nextButtonActive ? ColorReel.lightGreen : ColorReel.lightGray,
                  textStyle: buttonText18,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 12),
      ],
    );
  }
}
