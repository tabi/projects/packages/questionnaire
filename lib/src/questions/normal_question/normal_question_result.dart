import '../../definitions/question_result_base.dart';

// contains the answer from a normal question.
class NormalResult implements QuestionResultBase {
  // represents the question number.
  final int? _questionNumber;

  // represents the textfield answer.
  final String? _answer;

  @override
  int? get questionNumber {
    return _questionNumber;
  }

  String? get answer {
    return _answer;
  }

  NormalResult({
    required questionNumber,
    required answer,
  })  : _questionNumber = questionNumber,
        _answer = answer;

  @override
  String toString() {
    return 'Question Number [$questionNumber] with Answer [$answer]';
  }

  factory NormalResult.fromJson(Map<String, dynamic> data) => NormalResult(
        questionNumber: data["questionNumber"],
        answer: data["answer"],
      );

  Map<String, dynamic> toJson() {
    return {
      "questionNumber": questionNumber,
      "answer": answer,
    };
  }
}
