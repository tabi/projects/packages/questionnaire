
import '../../definitions/question_result_base.dart';

// contains the answer from a single choice question.
class SingleChoiceResult implements QuestionResultBase {
  // represents the question number.
  final int? _questionNumber;

  // represents the choice number which is selected.
  final int? _choiceNumber;

  // represents the optional textfield.
  String? _textField;

  SingleChoiceResult({
    required questionNumber,
    required choiceNumber,
    textField,
  })  : _questionNumber = questionNumber,
        _choiceNumber = choiceNumber,
        _textField = textField;

  @override
  int? get questionNumber {
    return _questionNumber;
  }

  int? get choiceNumber {
    return _choiceNumber;
  }

  String? get textField {
    return _textField;
  }

  void setTextField(String value) {
    _textField = value;
  }

  @override
  String toString() {
    var textString =
        this.textField != null ? ' and textfield [$textField]' : '';
    return 'Question Number [$questionNumber] with Choice [$choiceNumber]$textString';
  }

  factory SingleChoiceResult.fromJson(Map<String, dynamic> data) =>
      SingleChoiceResult(
        questionNumber: data["questionNumber"],
        choiceNumber: data["choiceNumber"],
        textField: data["textField"],
      );

  Map<String, dynamic> toJson() {
    return {
      "questionNumber": questionNumber,
      "choiceNumber": choiceNumber,
      "textField": textField,
    };
  }
}
