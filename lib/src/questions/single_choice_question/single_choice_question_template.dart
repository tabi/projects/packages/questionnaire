import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../definitions/choice_definition.dart';
import '../../mixins/animation_mixin.dart';
import '../../mixins/focus_mixin.dart';
import '../../widgets/always_disabled_focusnode.dart';
import 'single_choice_question_result.dart';

class SingleChoiceQuestion extends StatefulWidget {
  final SingleChoiceResult? value;
  final ValueChanged<SingleChoiceResult?> questionAnswered;
  final ValueChanged<SingleChoiceResult?> questionReturned;
  final String questionTranslationText;
  final String questionNextTranslationText;
  final String questionText;
  final String? hintText;
  final List<ChoiceDefinition> choiceList;
  final int questionNumber;
  final int numberInList;

  SingleChoiceQuestion({
    Key? key,
    required this.questionNumber,
    required this.choiceList,
    required this.questionText,
    required this.questionTranslationText,
    required this.questionNextTranslationText,
    required this.questionAnswered,
    required this.questionReturned,
    required this.numberInList,
    this.hintText,
    this.value,
  }) : super(key: key);

  @override
  _SingleChoiceQuestionState createState() => _SingleChoiceQuestionState();
}

class _SingleChoiceQuestionState extends State<SingleChoiceQuestion>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin, AnimationMixin, FocusAndTextControllerMixin {
  SingleChoiceResult? choiceClicked;
  AlwaysDisabledFocusNode? alwaysDisabledFocusNode;
  late bool hasNextButton;
  late bool nextButtonActive;
  late bool previousButtonActive;
  late List<ChoiceDefinition> choiceList;
  final scrollController = ScrollController();

  // used mainly for layout adjustments
  late bool hasNoTextfields;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    hasNextButton = false;
    hasNoTextfields = true;
    nextButtonActive = false;
    previousButtonActive = true;
    choiceList = widget.choiceList;
    createAnimation(tickerprovider: this);
    createFocusAndTextControllers(length: choiceList.length);
    alwaysDisabledFocusNode = AlwaysDisabledFocusNode();

    choiceClicked = widget.value ?? SingleChoiceResult(questionNumber: widget.questionNumber, choiceNumber: 0);

    if (choiceClicked!.choiceNumber != 0) {
      animationController.forward();
    }

    for (var choice in choiceList) {
      if (choice.addTextField) {
        textEditingControllers[choice.choiceNumber - 1] = TextEditingController();
        textFocusNodes[choice.choiceNumber - 1] = FocusNode();
        hasNoTextfields = false;
      }
    }

    // fill the textfield if there is one out of a stored answer.
    if (choiceClicked!.textField != null && choiceClicked!.textField!.isNotEmpty) {
      textEditingControllers[choiceClicked!.choiceNumber! - 1]!.text = choiceClicked!.textField!;

      setState(() {
        hasNextButton = true;
        nextButtonActive = true;
        previousButtonActive = true;
      });
    }
  }

  @override
  void dispose() {
    disposeFocusAndTextControllers(length: widget.choiceList.length);

    super.dispose();
  }

  void _resetFocusOnTextFields() {
    for (int c = 0; c != textFocusNodes.length; c++) {
      textFocusNodes[c]?.unfocus();
    }
  }

  Function? _checkNextButtonPressed() {
    if (nextButtonActive) {
      FocusScope.of(context).unfocus();
      widget.questionAnswered(choiceClicked);
      animationController.forward();
      return null;
    } else {
      return null;
    }
  }

  Function? _checkPreviousButtonPressed() {
    if (previousButtonActive) {
      FocusScope.of(context).unfocus();
      widget.questionReturned(choiceClicked);
      animationController.forward();
      return null;
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

//  Container(
//       margin: const EdgeInsets.only(bottom: 12),
//       padding: const EdgeInsets.symmetric(vertical: 17),
//       decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
//       child: child,
//     );

    return Column(
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(bottom: 12),
            padding: const EdgeInsets.symmetric(vertical: 17),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: RawScrollbar(
              controller: scrollController,
              thumbColor: Colors.grey,
              thickness: 8,
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: Text(
                        '${widget.questionTranslationText} ${widget.numberInList}'.toUpperCase(),
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
                      child: Text(
                        widget.questionText,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    (widget.hintText != null)
                        ? Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 12, right: 20, bottom: 15),
                            child: Text(widget.hintText!, style: Theme.of(context).textTheme.caption),
                          )
                        : Container(),
                    for (var item in choiceList)
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0, right: 20.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: hasNoTextfields ? 1 : 4,
                              child: Column(
                                children: [
                                  Listener(
                                      onPointerDown: (val) {
                                        setState(() {
                                          choiceClicked = SingleChoiceResult(
                                            questionNumber: widget.questionNumber,
                                            choiceNumber: item.choiceNumber,
                                          );
                                        });
                                        if (item.addTextField) {
                                          textFocusNodes[item.choiceNumber - 1]!.requestFocus();
                                          textEditingControllers[item.choiceNumber - 1]!.selection =
                                              TextSelection(baseOffset: 0, extentOffset: textEditingControllers[item.choiceNumber - 1]!.text.length);
                                          setState(() {
                                            hasNextButton = true;
                                            nextButtonActive = true;
                                          });
                                        } else {
                                          _resetFocusOnTextFields();
                                          setState(() {
                                            hasNextButton = false;
                                            nextButtonActive = true;
                                          });
                                        }
                                        setState(() {
                                          nextButtonActive = true;
                                        });
                                      },
                                      child: item.getVisible()
                                          ? RadioListTile(
                                              title: Text(
                                                item.choiceText,
                                                maxLines: 2,
                                                softWrap: true,
                                                style: Theme.of(context).textTheme.headline4,
                                              ),
                                              value: item.choiceNumber,
                                              groupValue: choiceClicked!.choiceNumber,
                                              onChanged: (dynamic value) {}, // This onChange handler is taken over by the Listener !!
                                            )
                                          : Container()),
                                ],
                              ),
                            ),
                            if (!hasNoTextfields)
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    item.addTextField
                                        ? TextFormField(
                                            style: choiceClicked!.choiceNumber == item.choiceNumber
                                                ? Theme.of(context).textTheme.headline4
                                                : Theme.of(context).textTheme.headline5,
                                            controller: textEditingControllers[item.choiceNumber - 1],
                                            focusNode: choiceClicked!.choiceNumber == item.choiceNumber
                                                ? textFocusNodes[item.choiceNumber - 1]
                                                : alwaysDisabledFocusNode,
                                            enabled: choiceClicked!.choiceNumber == item.choiceNumber,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(hintText: item.textFieldHint),
                                            onChanged: (value) {
                                              if (value?.isNotEmpty ?? false) {
                                                choiceClicked!.setTextField(value);
                                                textFocusNodes[item.choiceNumber - 1]!.unfocus();
                                                setState(() {
                                                  nextButtonActive = true;
                                                });
                                              } else {
                                                setState(() {
                                                  nextButtonActive = false;
                                                });
                                              }
                                            },
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    // hasNextButton
                    //     ? Padding(
                    //         padding: const EdgeInsets.only(right: 10),
                    //         child: OpacityButton(
                    //           buttonText: widget.questionNextTranslationText,
                    //           onPressedAction: _checkNextButtonPressed(),
                    //           opacityAnimation: opacityAnimation,
                    //         ),
                    //       )
                    //     : Container()
                  ],
                ),
              ),
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: _checkPreviousButtonPressed,
                child: Text('Vorige'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: ColorReel.lightGreen,
                  textStyle: buttonText18,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton(
                onPressed: _checkNextButtonPressed,
                child: Text('Volgende'),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: nextButtonActive ? ColorReel.lightGreen : ColorReel.lightGray,
                  textStyle: buttonText18,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 12,
        ),
      ],
    );
  }
}
