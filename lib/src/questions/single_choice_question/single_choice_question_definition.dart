import '../../definitions/choice_definition.dart';
import '../../definitions/question_base.dart';

class SingleChoiceQuestionDefinition implements QuestionBase {
  // represents the question type.
  final QuestionType _type;

  // contains the question number.
  final int _questionNumber;

  // contains the question text.
  final String _questionText;

  // contains the list of choices.
  final List<ChoiceDefinition> _choiceList;

  // contains a map where key = question number where to parse the answer from and value is a list of choices which should be hidden.
  // The first int in the list is the value of the answer we should check on !
  Map<int, List<int>> _hideConditions;

  // optional parameter contains the hint text.
  final String? _hintText;

  @override
  bool get hasNextButton {
    return false;
  }

  @override
  QuestionType get type {
    return _type;
  }

  @override
  int get questionNumber {
    return _questionNumber;
  }

  @override
  String get questionText {
    return _questionText;
  }

  List<ChoiceDefinition> get choiceList {
    return [..._choiceList];
  }

  Map<int, List<int>> get hideConditions {
    return {..._hideConditions};
  }

  String? get hintText {
    return _hintText;
  }

  SingleChoiceQuestionDefinition({
    type = QuestionType.singlechoice,
    required questionNumber,
    required questionText,
    required choiceList,
    hintText,
    hideConditions,
  })  : _type = type,
        _questionNumber = questionNumber,
        _questionText = questionText,
        _choiceList = choiceList,
        _hintText = hintText,
        _hideConditions = hideConditions ?? <int, List<int>>{};
}
