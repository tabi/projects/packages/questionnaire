import 'package:flutter/material.dart';

class OpacityButton extends StatelessWidget {
  final String? buttonText;
  final Function? onPressedAction;
  final Animation<double>? opacityAnimation;

  OpacityButton({
    this.buttonText,
    this.onPressedAction,
    this.opacityAnimation,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Align(
        alignment: Alignment.bottomRight,
        child: AnimatedBuilder(
          builder: (context, child) {
            return AnimatedOpacity(
              opacity: opacityAnimation!.value,
              duration: const Duration(milliseconds: 330),
              child: child,
            );
          },
          animation: opacityAnimation!,
          child: TextButton(
            onPressed: onPressedAction as void Function()?,
            child: Text(
              buttonText!,
            ),
          ),
        ),
      ),
    ]);
  }
}
