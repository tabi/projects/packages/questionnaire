import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';
import '../definitions/question_properties.dart';
import 'appbar.dart';
import 'pagination_widget.dart';
import 'question.dart';

final GlobalKey<PaginationWidgetState> _globalPaginationkey = GlobalKey();

class Questionnaire extends StatefulWidget {
  final int indexToJumpTo; // used when questionnaire is incomplete !
  final int itemCount;
  final IndexedWidgetBuilder itemBuilder;
  final String titleText;
  final Color captionColor;
  final Color buttonColor;
  final Color primaryTextColor;
  final Color backgroundColor;
  final GlobalKey<QuestionnaireState> questionnaireKey;
  final GlobalKey<PaginationWidgetState> _key = _globalPaginationkey;

  Questionnaire({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
    required this.indexToJumpTo,
    required this.titleText,
    required this.captionColor,
    required this.buttonColor,
    required this.primaryTextColor,
    required this.backgroundColor,
    required this.questionnaireKey,
  })  : assert(itemCount != null && itemCount > 0),
        assert(itemBuilder != null),
        assert(indexToJumpTo != null),
        assert(titleText != null),
        assert(captionColor != null),
        assert(buttonColor != null),
        assert(primaryTextColor != null),
        assert(backgroundColor != null),
        assert(questionnaireKey != null),
        super(key: key);

  @override
  QuestionnaireState createState() => QuestionnaireState();

  scrollPaginationWidget() {
    _key.currentState!.scrollPage();
  }

  resetPaginationWidget(int index) {
    _key.currentState!.makeActivePageVisible(index);
  }
}

class QuestionnaireState extends State<Questionnaire> with TickerProviderStateMixin {
  final _scrollController = PageController();
  int? itemCount;
  QuestionnaireAppBar? appBar;
  bool executeOnlyOnce = true;
  PaginationWidget? paginationWidget;

  @override
  void initState() {
    super.initState();

    paginationWidget = PaginationWidget(
      key: widget._key,
      animateToIndex: animateToIndex,
    );

    itemCount = widget.itemCount;
    appBar = QuestionnaireAppBar(
      totalWidgetCount: context.read<QuestionProperties>().getQuestionList.length,
      titleText: widget.titleText,
      backgroundColor: widget.backgroundColor,
    );
  }

  void nextQuestion(int index) {
    animateToIndex(index);

    Provider.of<QuestionProperties>(context, listen: false).setActivePage(index);

    if (index > 4) {
      widget.scrollPaginationWidget();
    }
  }

  void animateToIndex(int index, {bool notification = true}) {
    //_scrollController.animateToPage(index, duration: const Duration(milliseconds: 1000), curve: Curve);
    _scrollController.jumpToPage(index);
    Provider.of<QuestionProperties>(context, listen: false).setCurrentIndex(index, notification: notification);
  }

  void _makeSureActivePageIsVisible(BuildContext ctx, int activeIndex) {
    if (activeIndex > 7) {
      widget.resetPaginationWidget(activeIndex);
    }

    // print("_makeSureActivePageIsVisible, for Active Index [$activeIndex]");
    // var listVisibleProperties =
    //     Provider.of<QuestionProperties>(context, listen: false)
    //         .getPageProperties;
    // var activePageVisible =
    //     listVisibleProperties[activeIndex].visibilityPercentage > 95;
    // print(
    //     "_makeSureActivePageIsVisible, Active Page Visible [$activePageVisible]");

    // if (!activePageVisible) {
    //   var listVisibleIndices = Provider.of<QuestionProperties>(context,
    //           listen: false)
    //       .getPageProperties
    //       //.where((element) => element.visibilityPercentage != null && element.visibilityPercentage > 5)
    //       .toList();
    //   print(
    //       "_makeSureActivePageIsVisible, Active Indices List #[$listVisibleIndices]");
    //   var numberOfActiveIndices = listVisibleIndices.length;
    //   print(
    //       "_makeSureActivePageIsVisible, Active Indices #[$numberOfActiveIndices]");
    //   var midVisibleIndex = numberOfActiveIndices ~/ 2;
    //   print(
    //       "_makeSureActivePageIsVisible, Mid Visible Index #[$midVisibleIndex]");
    //   widget.resetPaginationWidget();
    //}
  }

  @override
  Widget build(BuildContext context) {
    itemCount = Provider.of<QuestionProperties>(context).getTotalWidgetCount + 1;

    if (executeOnlyOnce && widget.indexToJumpTo != 0) {
      WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
        animateToIndex(widget.indexToJumpTo + 1, notification: false);
        _makeSureActivePageIsVisible(context, widget.indexToJumpTo + 1);
        executeOnlyOnce = false;
      });
    }

    return Theme(
      data: Theme.of(context).copyWith(
        textTheme: Theme.of(context).textTheme.copyWith(
            caption: captionText,
            button: buttonText18,
            headline3: questionTopGreyText,
            headline4: optionText,
            headline5: hintText,
            headline6: questionMainTitleText),
      ),
      child: Builder(
        builder: (context) {
          return Scaffold(
            backgroundColor: widget.backgroundColor,
            appBar: appBar,
            body: Padding(
              padding: const EdgeInsets.only(left: 13, right: 13),
              child: PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                pageSnapping: true,
                scrollDirection: Axis.horizontal,
                controller: _scrollController,
                itemCount: itemCount,
                itemBuilder: (context, index) {
                  return (index != itemCount && index <= itemCount! - 1)
                      ? Consumer<QuestionProperties>(builder: (context, question, child) {
                          return Question(
                            key: index < question.getKeys!.length ? question.getKeys![index] : null,
                            active: index <= question.highestIndex,
                            child: widget.itemBuilder(context, index),
                          );
                        })
                      : Container();
                },
              ),
            ),
            bottomNavigationBar: paginationWidget,
          );
        },
      ),
    );
  }
}
