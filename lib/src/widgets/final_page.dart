import 'dart:async';

import 'package:flutter/material.dart';

import '../../questionnaire.dart';
import '../mixins/animation_stopped_mixin.dart';
import 'flare_animation.dart';

class FinalPage extends StatefulWidget implements QuestionBase {
  final String buttonText;
  final Color buttonColor;
  final String questionEndText;
  final String line1Text;
  final String line2Text;
  final String line3Text;
  final String answersSentText;
  final VoidCallback? onComplete;

  FinalPage({
    required this.buttonText,
    required this.buttonColor,
    required this.questionEndText,
    required this.line1Text,
    required this.line2Text,
    required this.line3Text,
    required this.answersSentText,
    required this.onComplete,
  });

  @override
  _FinalPageState createState() => _FinalPageState();

  @override
  bool get hasNextButton => false;

  @override
  int get questionNumber => 100;

  @override
  String get questionText => "";

  @override
  QuestionType get type => QuestionType.lastpage;
}

class _FinalPageState extends State<FinalPage> with AutomaticKeepAliveClientMixin, AnimationStoppedMixin {
  var showAnimation = false;
  var showButton = true;
  var showSendText = false;

  @override
  void onAnimationEnded() {
    setState(() {
      showSendText = true;
    });

    Timer _ = Timer(Duration(seconds: 2), () {
      widget.onComplete!();
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.symmetric(vertical: 17),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          showAnimation
              ? Expanded(
                  child: FlareAnimationWidget(delegate: this),
                )
              : Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Text(
                            widget.questionEndText.toUpperCase(),
                            style: questionTopGreyText,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            widget.line1Text,
                            style: questionMainTitleText,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            widget.line2Text,
                            style: questionBaseText,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 30),
                          child: Text(
                            widget.line3Text,
                            style: questionBaseText,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
          showButton
              ? Container(
                  margin: const EdgeInsets.only(top: 18, bottom: 20),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
                  width: 147,
                  height: 41,
                  child: Material(
                    borderRadius: BorderRadius.circular(5),
                    color: widget.buttonColor,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(5),
                      onTap: () {
                        setState(() {
                          showButton = false;
                          showAnimation = true;
                        });
                      },
                      child: Center(
                        child: Text(
                          widget.buttonText,
                          style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                )
              : showSendText
                  ? Container(
                      width: 147,
                      height: 41,
                      child: Center(
                        child: Text(
                          widget.answersSentText,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                    )
                  : Container(
                      width: 147,
                      height: 41,
                    ),
        ],
      ),
    );
  }
}
