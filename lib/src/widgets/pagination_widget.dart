import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';
import '../definitions/pagination_properties.dart';
import '../definitions/question_properties.dart';

class PaginationWidget extends StatefulWidget {
  final void Function(int)? animateToIndex;
  final textButtonWidth = 35.0;

  PaginationWidget({Key? key, this.animateToIndex}) : super(key: key);

  @override
  PaginationWidgetState createState() => PaginationWidgetState();
}

class PaginationWidgetState extends State<PaginationWidget> {
  final _scrollController = PageController();
  late List<PaginationProperties> _pages;
  bool _startMarkerReached = true;
  bool _endMarkerReached = false;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(_scrollListener);
    _pages = Provider.of<QuestionProperties>(context, listen: false).getPageProperties;
  }

  _scrollListener() {
    setState(() {
      _startMarkerReached = _scrollController.offset <= _scrollController.position.minScrollExtent && !_scrollController.position.outOfRange;

      _endMarkerReached = _scrollController.offset >= _scrollController.position.maxScrollExtent && !_scrollController.position.outOfRange;
    });
  }

  VoidCallback? _pageButtonClicked(int index) {
    if (_pages[index].isEnabled) {
      return () {
        widget.animateToIndex!(index);
        Provider.of<QuestionProperties>(context, listen: false).setActivePage(index);
      };
    } else {
      return null;
    }
  }

  VoidCallback? _startButtonPressed() {
    if (_startMarkerReached) {
      return null;
    } else {
      return () {
        _scrollController.animateTo(_scrollController.offset - widget.textButtonWidth, duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      };
    }
  }

  VoidCallback? _endButtonPressed() {
    if (_endMarkerReached) {
      return null;
    } else {
      return () {
        _scrollController.animateTo(_scrollController.offset + widget.textButtonWidth, duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      };
    }
  }

  void scrollPage() {
    _scrollController.jumpTo(_scrollController.offset + widget.textButtonWidth.toInt());
  }

  void makeActivePageVisible(int index) {
    var shift = (index - 4) * widget.textButtonWidth.toInt();
    _scrollController.animateTo(_scrollController.offset + shift, duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }

  BoxDecoration _displayButtonShadow(bool marker, {bool isStartMarker = true}) {
    return marker
        ? BoxDecoration()
        : BoxDecoration(
            boxShadow: [
              BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.25), blurRadius: 1.0, offset: isStartMarker ? Offset(1, 0) : Offset(-1, 0)),
              BoxShadow(color: Colors.white),
            ],
          );
  }

  @override
  Widget build(BuildContext context) {
    _pages = Provider.of<QuestionProperties>(context).getPageProperties;

    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        height: 40,
        color: Colors.white,
        child: Row(
          children: [
            // SizedBox(
            //   width: 40,
            //   child: Container(
            //     decoration: _displayButtonShadow(_startMarkerReached),
            //     child: IconButton(
            //         padding: EdgeInsets.zero,
            //         iconSize: 24,
            //         icon: Icon(
            //           Icons.arrow_back_ios_new,
            //         ),
            //         onPressed: _startButtonPressed()),
            //   ),
            // ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: _scrollController,
                itemCount: _pages.length,
                itemBuilder: (context, index) {
                  return Center(
                    child: SizedBox(
                      width: widget.textButtonWidth,
                      child: TextButton(
                        child: Text("${(index + 1).toString()}", style: TextStyle(fontSize: 15)),
                        style: _pages[index].isActive
                            ? TextButton.styleFrom(
                                foregroundColor: Colors.white,
                                backgroundColor: ColorReel.primaryColor,
                              )
                            : TextButton.styleFrom(
                                foregroundColor: Colors.black,
                              ),
                        onPressed: _pageButtonClicked(index),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(
              width: 8,
            ),
            // SizedBox(
            //   width: 40,
            //   child: Container(
            //     decoration: _displayButtonShadow(_endMarkerReached,
            //         isStartMarker: false),
            //     child: IconButton(
            //       padding: EdgeInsets.zero,
            //       iconSize: 24,
            //       icon: Icon(
            //         Icons.arrow_forward_ios,
            //       ),
            //       onPressed: _endButtonPressed(),
            //     ),
            //   ),
            // ),
          ],
        ),
      );
    });
  }
}
