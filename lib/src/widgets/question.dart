import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  final bool active;
  final Widget? child;

  const Question({required GlobalKey<State<StatefulWidget>>? key, required this.active, this.child})
      : assert(key != null),
        assert(active != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ColorFiltered(
      colorFilter: ColorFilter.mode(const Color.fromRGBO(0, 161, 205, 0.5), active ? BlendMode.dst : BlendMode.srcOver),
      child: child,
    );
  }
}
