import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../definitions/question_properties.dart';

class QuestionnaireAppBar extends StatefulWidget
    implements PreferredSizeWidget {
  final String titleText;
  final int totalWidgetCount;
  final Color backgroundColor;

  QuestionnaireAppBar({
    Key? key,
    required this.totalWidgetCount,
    required this.titleText,
    required this.backgroundColor,
  })  : preferredSize = Size.fromHeight(60),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _QuestionnaireAppBarState createState() => _QuestionnaireAppBarState();
}

class _QuestionnaireAppBarState extends State<QuestionnaireAppBar> {
  late int totalWidgetCount;
  late int currentIndex;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    totalWidgetCount =
        Provider.of<QuestionProperties>(context).getTotalWidgetCount;
    currentIndex = Provider.of<QuestionProperties>(context).getCurrentIndex;

    return currentIndex < totalWidgetCount
        ? AppBar(
            backgroundColor: widget.backgroundColor,
            automaticallyImplyLeading: false,
            elevation: 0,
            title: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Text(
                widget.titleText,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          )
        : Container();
  }
}
