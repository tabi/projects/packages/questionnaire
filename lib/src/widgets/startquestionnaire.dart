import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../questionnaire.dart';
import '../definitions/question_properties.dart';
import '../questions/multi_choice_question/multi_choice_question_template.dart';
import '../questions/multi_choice_with_icon_question/multi_choice_with_icon_question_template.dart';
import '../questions/normal_question/normal_question_template.dart';
import '../questions/person_info_question/person_info_question_template.dart';
import '../questions/single_choice_question/single_choice_question_template.dart';
import '../widgets/final_page.dart';
import '../widgets/introduction_page.dart';
import 'questionnaire.dart';

class StartQuestionnairePage extends StatefulWidget {
  final List<QuestionBase> _questionList;
  final Map<String, Color> _colorMap;
  final Map<String, String> _textMap;
  final OnQuestionnaireMixin _delegate;
  final Map<int, QuestionResultBase> _storedAnswers;

  StartQuestionnairePage({
    required questionList,
    required colorMap,
    required textMap,
    required delegate,
    required storedAnswers,
  })  : assert(questionList != null),
        assert(colorMap != null),
        assert(textMap != null),
        assert(delegate != null),
        _questionList = questionList,
        _colorMap = colorMap,
        _textMap = textMap,
        _delegate = delegate,
        _storedAnswers = storedAnswers;

  List<QuestionBase> get questions {
    return [..._questionList];
  }

  Color getColor(String txt) {
    return _colorMap[txt] ?? Colors.pink;
  }

  String getText(String txt) {
    return _textMap[txt] ?? "Undefined String";
  }

  int get latestIndexIncompleteQuestionnaire {
    int result = 0;
    if (_storedAnswers.length > 0) {
      var keys = _storedAnswers.keys.toList();
      keys.sort();
      var highestQuestionNumber = keys.last;
      var index = questions.indexWhere((element) => element.questionNumber == highestQuestionNumber);
      if (index != -1) {
        result = index;
      }
    }

    return result;
  }

  @override
  _StartQuestionnairePageState createState() => _StartQuestionnairePageState();
}

class _StartQuestionnairePageState extends State<StartQuestionnairePage> with TickerProviderStateMixin {
  var _answers = Map<int, QuestionResultBase>();
  final _questionnaireKey = GlobalKey<QuestionnaireState>();
  bool introductionPage = true;

  @override
  void initState() {
    super.initState();

    if (widget._storedAnswers.length > 0) {
      _answers = widget._storedAnswers;
    }
  }

  void animateToIndex(BuildContext context, int startIndex, int pageNumberIndex) {
    // check if we should remove question(s)
    if (pageNumberIndex - startIndex > 1) {
      _removeQuestions(context, startIndex + 1, pageNumberIndex);
    }

    _questionnaireKey.currentState!.nextQuestion(startIndex + 1);
  }

  void _removeQuestions(BuildContext context, int startIndex, int endIndex) {
    Provider.of<QuestionProperties>(context, listen: false).removeQuestions(startIndex, endIndex);
  }

  String _getQuestionText(PersonInfoQuestionDefinition questionDefinition) {
    var numberOfPersons = _getNumberOfPersonsFromQuestion(questionNumber: questionDefinition.questionNumberPersons);

    if (numberOfPersons == 1) {
      return questionDefinition.questionText3;
    }

    if (_getShowRelationInfoFromQuestion(
      relationMap: questionDefinition.relationMap,
    )) {
      return questionDefinition.questionText;
    }

    return questionDefinition.questionText2;
  }

  int _getNumberOfPersonsFromQuestion({int? questionNumber}) {
    var answerFromQuestion = _answers[questionNumber!] as NormalResult;
    return int.tryParse(answerFromQuestion.answer!) ?? 0;
  }

  List<ChoiceDefinition> _generateChoiceListText({int? questionNumber}) {
    var answerFromQuestion = _answers[questionNumber!] as PersonInfoResult;
    var numberOfPersons = answerFromQuestion.personList.length;

    final formatter = new DateFormat('dd MMMM yyyy');
    var newChoiceList = List<ChoiceDefinition>.empty(growable: true);

    for (int cnt = 0; cnt < numberOfPersons; cnt++) {
      var person = answerFromQuestion.personList[cnt];
      var dateString = formatter.format(person.dateOfBirth!);
      var personTextString = '${cnt + 1}. ${person.gender} - $dateString';
      var newChoiceDefinition = ChoiceDefinition(choiceNumber: cnt + 1, choiceText: personTextString);
      newChoiceList.add(newChoiceDefinition);
    }

    return newChoiceList;
  }

  bool _getShowRelationInfoFromQuestion({required Map<int, List<int>> relationMap}) {
    var answerNumber = relationMap.keys.first;
    var answerResult = _answers[answerNumber] as SingleChoiceResult?;
    var answerFromQuestion = answerResult?.choiceNumber;
    bool result = false;
    if (answerFromQuestion != null) {
      var res = relationMap[answerNumber];
      if (res != null && res.contains(answerFromQuestion)) {
        result = true;
      }
    }

    return result;
  }

  void _resetChoices(List<ChoiceDefinition> choices) {
    for (var choice in choices) {
      choice.setVisible(true);
    }
  }

  List<ChoiceDefinition> _processHideInfoFromQuestion(
      Map<int, Object> answers, int currentQuestionNumber, Map<int, List<int>> hideConditions, List<ChoiceDefinition> choices, BuildContext context) {
    List<int>? listOfHideableChoices = List<int>.empty(growable: true);

    _resetChoices(choices);

    if (hideConditions.length == 0) {
      return choices;
    }

    var numberOfQuestionToParseAnswerFrom = 0;

    if (hideConditions != null) {
      numberOfQuestionToParseAnswerFrom = hideConditions.entries.first.key;
    }

    if (numberOfQuestionToParseAnswerFrom == 0) {
      return choices;
    }

    var answer = answers[numberOfQuestionToParseAnswerFrom] as NormalResult?;

    if (hideConditions.length > 0) {
      listOfHideableChoices = hideConditions[numberOfQuestionToParseAnswerFrom];

      if (listOfHideableChoices != null && listOfHideableChoices.length > 0) {
        var intAnswer = int.tryParse(answer!.answer!) ?? 0;
        if (intAnswer != 0 && listOfHideableChoices.first == intAnswer) {
          listOfHideableChoices = listOfHideableChoices.sublist(1);
        } else {
          return choices;
        }
      }
    }

    for (var hideable in listOfHideableChoices!) {
      var t = choices.firstWhere((element) => element.choiceNumber == hideable, orElse: null);
      t?.setVisible(false);
    }

    return choices;
  }

  void _checkIfAlreadyAnswered(BuildContext context, int questionNumber) {
    var hasRemovedPages = Provider.of<QuestionProperties>(context, listen: false).hasRemovedPages;
    if (_answers[questionNumber] != null && hasRemovedPages) {
      Provider.of<QuestionProperties>(context, listen: false).resetQuestionList();
    }
  }

  void _resetChoicesIfAlreadyAnswered(
    BuildContext context,
    int questionNumber,
    List<ChoiceDefinition> resetChoices,
  ) {
    if (_answers[questionNumber] != null) {
      _resetChoices(resetChoices);
    }
  }

  int _getNextQuestionFromChoiceList({int? choiceNumber, required List<ChoiceDefinition> choiceList, required BuildContext context}) {
    var questionList = Provider.of<QuestionProperties>(context, listen: false).getQuestionList;
    var questionNumber = 0;
    var choice = choiceList.firstWhere((element) => element.choiceNumber == choiceNumber, orElse: null);
    questionNumber = choice.nextQuestionNumber ?? 0;

    // lookup questionNumber index in current question list
    var questionIndex = questionList.indexWhere((element) => element.questionNumber == questionNumber);
    if (questionIndex != -1) {
      return questionIndex;
    } else {
      return 0;
    }
  }

  int _getNextQuestionFromCondition({int? questionNumber, required BuildContext context}) {
    var questionList = Provider.of<QuestionProperties>(context, listen: false).getQuestionList;

    // lookup questionNumber index in current question list
    var questionIndex = questionList.indexWhere((element) => element.questionNumber == questionNumber);
    if (questionIndex != -1) {
      return questionIndex;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => QuestionProperties(widget.questions, widget.latestIndexIncompleteQuestionnaire)),
        ],
        builder: (context, wg) {
          var questionList = Provider.of<QuestionProperties>(context, listen: false).getQuestionList;
          for (var i in questionList) {}
          return !introductionPage
              ? Questionnaire(
                  key: _questionnaireKey,
                  indexToJumpTo: widget.latestIndexIncompleteQuestionnaire,
                  titleText: widget.getText(questionnaireCaptionText),
                  captionColor: widget.getColor(questionnaireCaptionColorText),
                  buttonColor: widget.getColor(questionnaireBlueButtonColorText),
                  primaryTextColor: widget.getColor(questionnairePrimaryTextColorText),
                  backgroundColor: widget.getColor(questionnaireBackgroundColorText),
                  questionnaireKey: _questionnaireKey,
                  itemCount: Provider.of<QuestionProperties>(context, listen: true).getQuestionList.length,
                  itemBuilder: (_, index) {
                    var questionList = Provider.of<QuestionProperties>(context, listen: true).getQuestionList;

                    final question = questionList[index];

                    switch (question.type) {
                      case QuestionType.normal:
                        final questionNormal = question as NormalQuestionDefinition;

                        var result = _answers[questionNormal.questionNumber] as NormalResult?;

                        return NormalQuestion(
                          subType: questionNormal.subType,
                          questionNumber: questionNormal.questionNumber,
                          value: result,
                          resumeActive: widget.latestIndexIncompleteQuestionnaire != 0,
                          numberInList: index + 1,
                          questionText: questionNormal.questionText,
                          hintTextfieldText: questionNormal.hintTextfieldText,
                          hintTitleText: questionNormal.hintTitleText,
                          hasNextButton: questionNormal.hasNextButton,
                          keyboardType: questionNormal.keyboardType,
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          conditions: questionNormal.conditions,
                          textFieldHeader: questionNormal.textFieldHeader,
                          textFieldIcon: questionNormal.textFieldIcon,
                          choice: questionNormal.choice,
                          uriLink: questionNormal.uriLink,
                          tokenHeader: questionNormal.tokenHeader,
                          questionReturned: (answer) {
                            if (index > 0) {
                              _questionnaireKey.currentState!.nextQuestion(index - 1);
                            }
                          },
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer);
                            _checkIfAlreadyAnswered(context, questionNormal.questionNumber);
                            _answers[questionNormal.questionNumber] = answer;
                            final conditionResult = questionNormal.conditions[answer.answer!];
                            final checkRoute = questionNormal.conditions['Route'];
                            var nextQuestionNumberIndex = 0;
                            if (checkRoute != null) {
                              nextQuestionNumberIndex = checkRoute;
                            } else {
                              if (conditionResult == null && questionNormal.conditions['Other'] != null) {
                                nextQuestionNumberIndex = _getNextQuestionFromCondition(questionNumber: questionNormal.conditions['Other'], context: context);
                              } else {
                                nextQuestionNumberIndex = _getNextQuestionFromCondition(questionNumber: conditionResult ?? 0, context: context);
                              }
                            }
                            nextQuestionNumberIndex != 0
                                ? animateToIndex(context, index, nextQuestionNumberIndex)
                                : _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.singlechoice:
                        final questionSingle = question as SingleChoiceQuestionDefinition;
                        assert(questionSingle.choiceList.length > 1);

                        var result = _answers[questionSingle.questionNumber] as SingleChoiceResult?;

                        return SingleChoiceQuestion(
                          questionNumber: questionSingle.questionNumber,
                          value: result,
                          numberInList: index + 1,
                          choiceList: _processHideInfoFromQuestion(_answers, questionSingle.questionNumber, questionSingle.hideConditions,
                              (questionSingle.choiceList ?? {}) as List<ChoiceDefinition>, context),
                          questionText: questionSingle.questionText,
                          hintText: questionSingle.hintText ?? null,
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          questionReturned: (answer) {
                            if (index > 0) {
                              _questionnaireKey.currentState!.nextQuestion(index - 1);
                            }
                          },
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer!);
                            //_checkIfAlreadyAnswered(context, questionSingle.questionNumber);
                            //_resetChoicesIfAlreadyAnswered(context, questionSingle.questionNumber, questionSingle.choiceList);
                            _answers[questionSingle.questionNumber] = answer;
                            _questionnaireKey.currentState!.nextQuestion(index + 1);
                            // final nextQuestionNumberIndex =
                            //     _getNextQuestionFromChoiceList(choiceNumber: answer.choiceNumber, choiceList: questionSingle.choiceList, context: context);
                            // nextQuestionNumberIndex != 0
                            //     ? animateToIndex(context, index, nextQuestionNumberIndex)
                            //     : _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.multichoice:
                        final questionMultiple = question as MultipleChoiceQuestionDefinition;
                        assert(questionMultiple.choiceList.length > 1);

                        var result = _answers[questionMultiple.questionNumber] as MultiChoiceResult?;

                        return MultipleChoiceQuestion(
                          questionNumber: questionMultiple.questionNumber,
                          value: result?.resultList,
                          numberInList: index + 1,
                          choiceList: questionMultiple.choiceList,
                          questionText: questionMultiple.questionText,
                          hintText: questionMultiple.hintText ?? null,
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          questionReturned: (answer) {
                            if (index > 0) {
                              _questionnaireKey.currentState!.nextQuestion(index - 1);
                            }
                          },
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer!);
                            _answers[questionMultiple.questionNumber] = answer;
                            _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.multichoice_icon:
                        final questionMultipleIcon = question as MultipleChoiceWithIconQuestionDefinition;
                        assert(questionMultipleIcon.choiceList.length > 1);
                        assert(questionMultipleIcon.iconList.length > 1);

                        var result = _answers[questionMultipleIcon.questionNumber] as MultiChoiceResult?;

                        return MultipleChoiceWithIconQuestion(
                          questionNumber: questionMultipleIcon.questionNumber,
                          value: result?.resultList,
                          numberInList: index + 1,
                          choiceList: questionMultipleIcon.choiceList,
                          iconList: questionMultipleIcon.iconList,
                          questionText: questionMultipleIcon.questionText,
                          hintText: questionMultipleIcon.hintText ?? null,
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          questionReturned: (answer) {
                            if (index > 0) {
                              _questionnaireKey.currentState!.nextQuestion(index - 1);
                            }
                          },
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer!);
                            _answers[questionMultipleIcon.questionNumber] = answer;
                            _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.personeducation:
                        final questionPersonEducation = question as PersonEducationChoiceQuestion;

                        var result = _answers[questionPersonEducation.questionNumber] as MultiChoiceResult?;

                        return MultipleChoiceQuestion(
                          questionNumber: questionPersonEducation.questionNumber,
                          value: result?.resultList,
                          numberInList: index + 1,
                          choiceList: _generateChoiceListText(questionNumber: questionPersonEducation.questionNumberPersons),
                          questionText: questionPersonEducation.questionText,
                          hintText: questionPersonEducation.hintText ?? null,
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          questionReturned: (answer) {
                            if (index > 0) {
                              _questionnaireKey.currentState!.nextQuestion(index - 1);
                            }
                          },
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer!);
                            _answers[questionPersonEducation.questionNumber] = answer;
                            _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.personinfo:
                        final questionPersonInfo = question as PersonInfoQuestionDefinition;
                        assert(questionPersonInfo.relationMap.length > 0);
                        assert(questionPersonInfo.relationChoices.length > 1);
                        assert(questionPersonInfo.genderChoices.length > 0);

                        var result = _answers[questionPersonInfo.questionNumber] as PersonInfoResult?;

                        return PersonInfoChoiceQuestion(
                          questionNumber: questionPersonInfo.questionNumber,
                          value: result?.personList,
                          numberInList: index + 1,
                          questionText: _getQuestionText(questionPersonInfo),
                          questionTranslationText: widget.getText(questionnaireQuestionText),
                          questionNextTranslationText: widget.getText(questionnaireNextButtonText),
                          hasNextButton: questionPersonInfo.hasNextButton,
                          inputDataNumberOfPersons: _getNumberOfPersonsFromQuestion(questionNumber: questionPersonInfo.questionNumberPersons),
                          showRelation: _getShowRelationInfoFromQuestion(
                            relationMap: questionPersonInfo.relationMap,
                          ),
                          titleText: questionPersonInfo.titleText,
                          backgroundColor: widget.getColor(questionnaireBackgroundColorText),
                          relationMap: questionPersonInfo.relationMap,
                          relationChoices: questionPersonInfo.relationChoices,
                          yourselfText: widget.getText(questionnairePersonInfoYourSelfText),
                          genderChoices: questionPersonInfo.genderChoices,
                          genderHintText: widget.getText(questionnairePersonInfoGenderHintText),
                          genderDialogText: widget.getText(questionnairePersonInfoGenderDialogText),
                          dateOfBirthHintText: widget.getText(questionnairePersonInfoDateOfBirthText),
                          dateOfBirthDialogText: widget.getText(questionnairePersonInfoDateOfBirthText),
                          relationHintText: widget.getText(questionnairePersonInfoRelationHintText),
                          relationText: widget.getText(questionnairePersonInfoRelationText),
                          confirmButtonText: widget.getText(questionnaireConfirmButtonText),
                          dateInThePastText: widget.getText(questionnaireCalendarDateInThePastText),
                          questionAnswered: (answer) {
                            widget._delegate.onQuestionComplete(answer!);
                            _answers[questionPersonInfo.questionNumber] = answer;
                            _questionnaireKey.currentState!.nextQuestion(index + 1);
                          },
                        );
                      case QuestionType.lastpage:
                        return FinalPage(
                            buttonText: widget.getText(questionnaireSaveButtonText),
                            buttonColor: widget.getColor(questionnaireGreenButtonColorText),
                            questionEndText: widget.getText(questionnaireLastPageEndText),
                            line1Text: widget.getText(questionnaireLastPageLine1Text),
                            line2Text: widget.getText(questionnaireLastPageLine2Text),
                            line3Text: widget.getText(questionnaireLastPageLine3Text),
                            answersSentText: widget.getText(questionnaireAnswersSentText),
                            onComplete: () async {
                              widget._delegate.onQuestionnaireComplete(_answers);
                              Navigator.of(context).pop();
                            });

                      default:
                        throw UnsupportedError('QuestionType not supported');
                    }
                  },
                )
              : IntroductionPage(
                  titleText: widget.getText(questionnaireCaptionText),
                  backgroundColor: widget.getColor(questionnaireBackgroundColorText),
                  buttonColor: widget.getColor(questionnaireGreenButtonColorText),
                  buttonText: widget.getText(questionnaireStartButtonText),
                  questionIntroText: widget.getText(questionnaireIntroText),
                  line1Text: widget.getText(questionnaireIntroductionPageLine1Text),
                  line2Text: widget.getText(questionnaireIntroductionPageLine2Text),
                  line3Text: widget.getText(questionnaireIntroductionPageLine3Text),
                  onButtonTap: () {
                    setState(() {
                      introductionPage = false;
                    });
                  },
                );
        },
      ),
    );
  }
}
