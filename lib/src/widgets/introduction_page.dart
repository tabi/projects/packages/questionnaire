import 'package:flutter/material.dart';

import '../../constants.dart';

class IntroductionPage extends StatelessWidget {
  final String titleText;
  final String buttonText;
  final Color backgroundColor;
  final Color buttonColor;
  final String questionIntroText;
  final String line1Text;
  final String line2Text;
  final String line3Text;
  final VoidCallback onButtonTap;

  const IntroductionPage(
      {Key? key,
      required this.line1Text,
      required this.line2Text,
      required this.line3Text,
      required this.questionIntroText,
      required this.titleText,
      required this.buttonText,
      required this.buttonColor,
      required this.backgroundColor,
      required this.onButtonTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: Padding(
        padding: const EdgeInsets.only(left: 20, top: 40, right: 20),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                titleText,
                style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w500, fontFamily: 'Soho Pro'),
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 25, bottom: 10),
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          questionIntroText.toUpperCase(),
                          style: questionTopGreyText,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text(
                            line1Text,
                            softWrap: true,
                            style: questionMainTitleText,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Text(
                            line2Text,
                            style: questionBaseText,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 20),
                          child: Text(
                            line3Text,
                            style: questionBaseText,
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ElevatedButton(
                onPressed: onButtonTap,
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(1, 45),
                  foregroundColor: ColorReel.lightGreen,
                  textStyle: buttonText18,
                ),
                child: Text(
                  buttonText,
                  style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              height: 38,
            )
          ],
        ),
      ),
    );
  }
}
