import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';

import '../mixins/animation_stopped_mixin.dart';

class FlareAnimationWidget extends StatelessWidget {
  final AnimationStoppedMixin? delegate;

  FlareAnimationWidget({this.delegate});

  void onAnimationCompleted(String name) {
    delegate!.onAnimationEnded();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.white),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 250,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 250,
                  child: FlareActor(
                      "packages/questionnaire/assets/sending.flr",
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      callback: onAnimationCompleted,
                      animation: "Untitled"),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
